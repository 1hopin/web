import PropTypes from 'prop-types'
import { useEffect } from 'react'
import toast, { Toaster } from 'react-hot-toast'

export default function Toast({ options }) {
  useEffect(() => {
    const { type, msg } = options
    if (type === 'error') toast.error(msg, { duration: 4000 })
    else toast.success(msg, { duration: 4000 })
  }, [options.id])

  return <Toaster position="top-center" />
}

Toast.propTypes = {
  options: PropTypes.object.isRequired,
}
