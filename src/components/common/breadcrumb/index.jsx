import Link from 'next/link'
import PropTypes from 'prop-types'
import { Fragment } from 'react'
import { useTranslation } from 'react-i18next'

import Icon from '../icon'
import styles from './breadcrumb.module.scss'

export default function Breadcrumb({ breadcrumb }) {
  const { t } = useTranslation()
  return (
    <div className={styles['wrapper__breadcrumb']}>
      <nav className={styles['breadcrumb']}>
        <Link href="/">
          <a>{t('common:breadcrumb-home')}</a>
        </Link>
        {breadcrumb.map(([route, params, text]) => {
          return (
            <Fragment key={route}>
              <span>
                <Icon path="chevron-right" w="14" h="14" />
              </span>
              <Link href={`/${route}/${params.id}`}>
                <a>{text}</a>
              </Link>
            </Fragment>
          )
        })}
      </nav>
    </div>
  )
}

Breadcrumb.propTypes = {
  breadcrumb: PropTypes.array,
}
