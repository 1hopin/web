import PropTypes from 'prop-types'

import styles from './button.module.scss'

export default function PrimaryButton({ title, active = true }) {
  return (
    <button
      className={`${styles['btn']} ${!active ? styles['btn--disable'] : styles['btn--primary']}`}
    >
      {title}
    </button>
  )
}

PrimaryButton.propTypes = {
  active: PropTypes.bool,
  title: PropTypes.string.isRequired,
}
