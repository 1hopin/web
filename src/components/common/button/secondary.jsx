import PropTypes from 'prop-types'

import styles from './button.module.scss'

export default function SecondaryButton({ title, available, active, onBtnClick = () => {} }) {
  return (
    <button
      className={`${styles['btn']} ${
        !available
          ? styles['btn--fade']
          : active
          ? `${styles['btn--nd']} ${styles['btn--active']}`
          : styles['btn--nd']
      }`}
      onClick={() => {
        onBtnClick()
      }}
    >
      {title}
    </button>
  )
}

SecondaryButton.propTypes = {
  title: PropTypes.string.isRequired,
  active: PropTypes.bool,
  available: PropTypes.bool,
  onBtnClick: PropTypes.func,
}
