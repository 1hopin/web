import PropTypes from 'prop-types'

import svgIcon from '../../utils/icon/svgIcon.json'

export default function Icon({ path, w, h, gw, gh, fill, strokeWidth, className, children }) {
  const iconW = w || '19'
  const iconH = h || '19'

  const gridW = gw || '24'
  const gridH = gh || '24'

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={iconW}
      height={iconH}
      fill={fill}
      viewBox={`0 0 ${gridW} ${gridH}`}
      className={className}
      style={{ strokeWidth }}
    >
      {path && <path d={svgIcon[path]} style={{ stroke: 'currentcolor', fill: 'currentcolor' }} />}
      {children}
    </svg>
  )
}

Icon.propTypes = {
  path: PropTypes.string,
  children: PropTypes.node,
  fill: PropTypes.string,
  strokeWidth: PropTypes.string,
  className: PropTypes.string,
  w: PropTypes.string,
  h: PropTypes.string,
  gw: PropTypes.string,
  gh: PropTypes.string,
}
