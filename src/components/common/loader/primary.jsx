import styles from './loader.module.scss'

export default function LoaderPrimary() {
  return <div className={styles['loader']}></div>
}
