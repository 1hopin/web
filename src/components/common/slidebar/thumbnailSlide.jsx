import '@splidejs/splide/dist/css/themes/splide-default.min.css'

import { Splide, SplideSlide } from '@splidejs/react-splide'
import Image from 'next/image'
import PropTypes from 'prop-types'
import React, { useEffect } from 'react'

import styles from './slide.module.scss'

export default function ThumbnailSlide({ data, options, priority = false }) {
  const imageHeight = options?.image?.height || 400
  const imageWidth = options?.image?.width || 1440 // default = max screen width

  const mainRef = React.createRef(<Splide />)
  const thumbsRef = React.createRef(<Splide />)

  useEffect(() => {
    if (mainRef.current && thumbsRef.current && thumbsRef.current.splide) {
      mainRef.current.sync(thumbsRef.current.splide)
    }
  }, [])

  const mainOptions = {
    type: 'fade',
    rewind: 'true',
    pagination: false,
    arrows: false,
    width: 445,
    focus: 'center',
    padding: '10%',
  }

  const thumbsOptions = {
    type: 'slide',
    rewind: true,
    fixedWidth: 60,
    fixedHeight: 60,
    gap: '1rem',
    pagination: false,
    isNavigation: true,
    cover: true,
    width: 445,
    perPage: 5,
    breakpoints: {
      600: {
        fixedWidth: 60,
        fixedHeight: 44,
      },
    },
  }

  return (
    <div className={styles['product__gallery--wrapper']}>
      <Splide options={mainOptions} ref={mainRef}>
        {data.map(({ image, title }, index) => (
          <SplideSlide key={`${title}_${index}`}>
            <Image
              src={image}
              alt={title}
              width={imageWidth}
              height={imageHeight}
              priority={priority}
            />
          </SplideSlide>
        ))}
      </Splide>
      <Splide options={thumbsOptions} ref={thumbsRef}>
        {data.map(({ image, title, url }, index) => (
          <SplideSlide key={`${title}_${index}`}>
            <Image src={image} alt={title} width={100} height={100} priority={priority} />
          </SplideSlide>
        ))}
      </Splide>
    </div>
  )
}

ThumbnailSlide.propTypes = {
  data: PropTypes.array,
  options: PropTypes.object,
  priority: PropTypes.bool,
}
