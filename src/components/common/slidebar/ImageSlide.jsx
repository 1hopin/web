import '@splidejs/splide/dist/css/themes/splide-default.min.css'

import { Splide, SplideSlide } from '@splidejs/react-splide'
import Image from 'next/image'
import Link from 'next/link'
import PropTypes from 'prop-types'

export default function ImageSlide({ data, options, priority = false }) {
  const imageHeight = options?.image?.height || 400
  const imageWidth = options?.image?.width || 1184 // default = max screen width
  // const
  return (
    <Splide
      options={{
        type: 'loop',
        autoplay: true,
        rewind: true,
        interval: 4000,
        pauseOnHover: true,
        arrows: false,
        ...options,
      }}
    >
      {data.map(({ image, title, url }, index) => (
        <SplideSlide key={`${title}_${index}`}>
          <Link href={url}>
            <a>
              <Image
                src={image}
                alt={title}
                width={imageWidth}
                height={imageHeight}
                priority={priority}
              />
            </a>
          </Link>
        </SplideSlide>
      ))}
    </Splide>
  )
}

ImageSlide.propTypes = {
  data: PropTypes.array,
  options: PropTypes.object,
  priority: PropTypes.bool,
}
