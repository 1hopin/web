import '@splidejs/splide/dist/css/themes/splide-default.min.css'

import ProductCard from '@components/common/product/productCard'
import { Splide, SplideSlide } from '@splidejs/react-splide'
import { useDevice } from '@utils/userAgent'
import PropTypes from 'prop-types'

import styles from './slide.module.scss'

export default function ProductSlide({ items, isFlashDeal }) {
  const { isMobile } = useDevice()

  // If product is less than 6 items, return component without splide
  if (items?.length <= 6) {
    return (
      <div className={styles['product__group']}>
        {items?.map((item, index) => (
          <div key={index} className={styles['product__group-col']}>
            <ProductCard productDetail={item} />
          </div>
        ))}
      </div>
    )
  }

  return (
    <div className={styles.slidebar}>
      <Splide
        options={{
          perPage: !isMobile ? 6 : 2,
          perMove: !isMobile ? 3 : 1,
        }}
      >
        {items?.map((item, index) => {
          return (
            <SplideSlide key={index}>
              <ProductCard productDetail={item} />
            </SplideSlide>
          )
        })}
      </Splide>
    </div>
  )
}

ProductSlide.propTypes = {
  items: PropTypes.array,
  isFlashDeal: PropTypes.bool,
}
