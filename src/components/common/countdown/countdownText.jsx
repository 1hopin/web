import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'

import styles from './countdownText.module.scss'

export default function CountdownText({ days, hours, minutes, seconds }) {
  const [startRender, setStartRender] = useState(false)
  useEffect(() => {
    setStartRender(true)
  })

  return (
    startRender && (
      <div className={styles['clock__container']}>
        <span className={styles['countdown-text']}>{`${days + 'D'}`}</span>:
        <span className={styles['countdown-text']}>{`${addZero(hours)}`}</span>:
        <span className={styles['countdown-text']}>{`${addZero(minutes)}`}</span>:
        <span className={styles['countdown-text']}>{`${addZero(seconds)}`}</span>
      </div>
    )
  )
}

function addZero(num = 0) {
  return num < 10 ? `0${num}` : num
}

CountdownText.propTypes = {
  days: PropTypes.number,
  hours: PropTypes.number,
  minutes: PropTypes.number,
  seconds: PropTypes.number,
}
