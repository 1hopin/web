import PropTypes from 'prop-types'

import styles from './home.module.scss'

export default function HomeMobile(props) {
  return (
    <div className={styles.container}>
      <div className={styles.home}>Home Page</div>
    </div>
  )
}

HomeMobile.propTypes = {
  categoryList: PropTypes.array,
  productOnHomePage: PropTypes.array,
}
