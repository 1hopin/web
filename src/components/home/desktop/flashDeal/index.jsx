import CountdownText from '@components/common/countdown/countdownText'
import Icon from '@components/common/icon'
import ProductSlide from '@components/common/slidebar/productSlide'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import styles from './flashDeal.module.scss'

export default function FlashDealSection({ data, countdown }) {
  const { t, i18n } = useTranslation('common')
  const {
    campaign: { title_th, title_en },
    products_summary: { products },
  } = data

  if (countdown.completed) return null
  return (
    <div className={styles['flash-deal__container']}>
      <div className="container">
        <div className={styles['header__section']}>
          <div className={styles['flash-deal__title']}>
            <Icon
              path="thunder"
              w="24"
              h="24"
              gw="23"
              gh="23"
              strokeWidth="0.5"
              className={styles['icon-thunder']}
            />
            {i18n.language === 'th' ? title_th : title_en}
            <CountdownText {...countdown} />
          </div>
          {products?.length > 6 && (
            <div className={styles['flash-deal__seemore']}>
              {t('section-seemore')}
              <i className={styles['flash-deal__seemore-icon']}>
                <svg viewBox="0 0 40 40">
                  <path d="m15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z"></path>
                </svg>
              </i>
            </div>
          )}
        </div>
        <ProductSlide items={products} isFlashDeal={true} />
      </div>
    </div>
  )
}

FlashDealSection.propTypes = {
  data: PropTypes.object,
  countdown: PropTypes.object,
}
