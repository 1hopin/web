import ImageSlide from '@components/common/slidebar/ImageSlide'
import { useDevice } from '@utils/userAgent'

import styles from './banner.module.scss'

export default function PromotionBanner() {
  const { isMobile } = useDevice()
  const banners = [
    {
      title: 'promotion1',
      image: '/images/mock/promotions/banner1.png',
      url: '/',
    },
    {
      title: 'promotion2',
      image: '/images/mock/promotions/banner2.png',
      url: '/',
    },
    {
      title: 'promotion3',
      image: '/images/mock/promotions/banner3.png',
      url: '/',
    },
    {
      title: 'promotion4',
      image: '/images/mock/promotions/banner4.png',
      url: '/',
    },
    {
      title: 'promotion5',
      image: '/images/mock/promotions/banner2.png',
      url: '/',
    },
    {
      title: 'promotion6',
      image: '/images/mock/promotions/banner3.png',
      url: '/',
    },
  ]

  const slideOption = {
    pagination: false,
    perPage: !isMobile ? 4 : 3,
    autoScroll: {
      speed: 2,
    },
    gap: '1rem',
    image: {
      width: !isMobile ? 340 : 240,
      height: !isMobile ? 150 : 110,
    },
  }

  return (
    <div className={styles['banner__promotional']}>
      <ImageSlide data={banners} options={slideOption} priority />
    </div>
  )
}
