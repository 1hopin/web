import Image from 'next/image'
import Link from 'next/link'
import PropTypes from 'prop-types'

import styles from './banner.module.scss'

export default function ContentBannerSection({ banners }) {
  return (
    <div className="container">
      <div className={styles['banners__wrapper']}>
        {banners?.map((banner, index) => (
          <Link href={banner?.url} key={index}>
            <a className={styles['banners__wrapper-banner']}>
              <Image
                src={banner?.image}
                alt="Banner1"
                width={banner?.width}
                height={banner?.height}
              />
            </a>
          </Link>
        ))}
      </div>
    </div>
  )
}

ContentBannerSection.propTypes = {
  banners: PropTypes.array,
}
