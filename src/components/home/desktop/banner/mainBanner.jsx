// import ImageSlide from '@components/common/slidebar/ImageSlide'

import { useDevice } from '@utils/userAgent'
import Image from 'next/image'

export default function MainBanner() {
  const { isMobile } = useDevice()
  const banners = [
    {
      title: 'Banner1',
      image: '/images/mock/home/banner1.webp',
      url: '/',
    },
    {
      title: 'Banner2',
      image: '/images/mock/home/banner1.webp',
      url: '/',
    },
    {
      title: 'Banner3',
      image: '/images/mock/home/banner1.webp',
      url: '/',
    },
    {
      title: 'Banner4',
      image: '/images/mock/home/banner1.webp',
      url: '/',
    },
  ]

  let slideOptions = {
    width: 597,
    height: 336,
    image: {
      width: 597,
      height: 336,
    },
  }

  return (
    <Image
      src={`/images/mock/home/${isMobile ? 'banner1_mobile.jpeg' : 'banner1.webp'}`}
      alt="Banner1"
      width={597}
      height={336}
      priority={true}
    />
  )
}
