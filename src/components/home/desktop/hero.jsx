import Icon from '@components/common/icon'
import clsx from 'clsx'
import Image from 'next/image'
import Link from 'next/link'
import PropTypes from 'prop-types'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import MainBanner from './banner/mainBanner'
import styles from './hero.module.scss'

export default function DesktopHero({ parentCategoryList }) {
  const { i18n } = useTranslation()
  const [activeParentIndex, setActiveParentIndex] = useState(null)
  const [isShowDropdown, setIsShowDropdown] = useState(false)

  const DropDownContent = () => {
    return (
      <div className={styles['hp__wrapper-content']}>
        <div className={styles['dropdown-content']}>
          {parentCategoryList[activeParentIndex].name && (
            <div className={styles.header}>
              <h2>{parentCategoryList[activeParentIndex].name} </h2>
            </div>
          )}

          <div className={styles.row}>
            {parentCategoryList[activeParentIndex].children.map((child, index) => {
              return (
                <div key={`child1_${index}`} className={styles.column}>
                  <h3>{child.name}</h3>
                  {child.children.map((subChild, index) => (
                    <Link key={`category_child_${index}`} href={`/category/${subChild.id}`}>
                      <a>{subChild.name}</a>
                    </Link>
                  ))}
                </div>
              )
            })}
          </div>
        </div>
      </div>
    )
  }

  const onHoverParentCategory = ({ index }) => {
    setActiveParentIndex(index)
    if (isShowDropdown === false) {
      setIsShowDropdown(true)
    }
  }

  return (
    <section className={clsx('container', styles['hp__wrapper--hero'])}>
      <div className={`${styles['hp__categories']}`}>
        <div className={`${styles['hp__categories--title']}`}>
          หมวดหมู่สินค้า
          <Icon
            path="list"
            w="20"
            h="20"
            strokeWidth="0.5"
            className={styles['icon__list--white']}
          />
        </div>
        <div
          className={styles['hp__categories--wrapper']}
          onMouseLeave={() => setIsShowDropdown(false)}
        >
          <div className={styles['sidebar-container']}>
            <div className={styles['sidebar']}>
              {parentCategoryList.length > 0 &&
                parentCategoryList.map((val, index) => {
                  return (
                    <div
                      className={`${styles['sidebar__item']}`}
                      key={`parentCategoryList_${index}`}
                    >
                      <div className={styles['icon__wrapper-circle']}>
                        <Icon
                          path="truck"
                          w="28"
                          h="28"
                          className={styles['icon__bigc--circle']}
                          strokeWidth="0.25"
                        />
                      </div>
                      <div
                        className={`${styles['wrapper__categories--item']}`}
                        onMouseOver={() => onHoverParentCategory({ index: index, id: val.id })}
                      >
                        <Link href={`/category/${val.id}`}>
                          <a>
                            <div className={`${styles['categories']}`}>
                              <div className={`${styles['categories__name']}`}>
                                {i18n.language === 'en' ? val.name_en : val.name}
                              </div>
                              <Icon
                                path="chevron-right"
                                w="8"
                                h="8"
                                gw="16"
                                gh="16"
                                strokeWidth="3"
                                className={styles['icon__chevron--right']}
                              />
                            </div>
                          </a>
                        </Link>
                      </div>
                    </div>
                  )
                })}
            </div>
          </div>

          <div className={styles['hp__wrapper-spacer']} />

          {isShowDropdown && <DropDownContent />}
        </div>
      </div>
      <div className={`${styles['hp__wrapper--banner']}`}>
        <MainBanner />
      </div>
      <div className={`${styles['hp__wrapper--thumb']}`}>
        <Image src={'/images/mock/home/banner2_1.webp'} width={240} height={160} />
        <Image src={'/images/mock/home/banner2_2.webp'} width={240} height={160} />
      </div>
    </section>
  )
}

DesktopHero.propTypes = {
  parentCategoryList: PropTypes.array,
}
