import Image from 'next/image'

import styles from './homeFooter.module.scss'

export default function HomeFooter() {
  const promoteList = [
    {
      title: 'สั่งง่าย อยู่ที่ไหนก็ช้อปได้',
      desc: 'สะดวกทุกที่ทุกเวลา ช้อปผ่านทางเว็บไซต์และแอปพลิเคชัน',
      image: '/images/footer/promote_1.png',
    },
    {
      title: 'ส่งไว ทันใจ',
      desc: 'จัดส่งสินค้า ทั้งแบบส่งด่วน 1 ชม. หรือแบบภายใน 24 ชม.',
      image: '/images/footer/promote_2.png',
    },
    {
      title: 'ครบตรงใจคุณ',
      desc: 'คัดสรรสินค้าคุณภาพ หลากหลาย ครบทุกความต้องการ',
      image: '/images/footer/promote_3.png',
    },
    {
      title: 'สิทธิประโยชน์มากมาย',
      desc: 'สะสมคะแนน แลกรับสิทธิพิเศษ ตอบโจทย์ไลฟ์สไตล์ที่เป็นคุณ',
      image: '/images/footer/promote_4.png',
    },
    {
      title: 'ช่องทางการชำระเงิน',
      desc: 'หลากหลายช่องทางให้คุณได้เลือก มีบริการเก็บเงินปลายทาง',
      image: '/images/footer/promote_5.png',
    },
  ]

  return (
    <section className={styles.container}>
      <div className="container">
        <div className={styles['header__section']}>
          <div className={styles['title']}>มาช้อปที่บิ๊กซีออนไลน์</div>
        </div>

        <div className={styles.row}>
          {promoteList?.map((category, index) => {
            return (
              <div className={styles.col} key={index}>
                <Image src={category?.image} width={216} height={168} />
                <div className={styles.title}>{category?.title}</div>
                <p className={styles.barrier} />
                <div className={styles.desc}>{category?.desc}</div>
              </div>
            )
          })}
        </div>
      </div>
    </section>
  )
}
