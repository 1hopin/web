import clsx from 'clsx'
import Image from 'next/image'
import Link from 'next/link'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import styles from './category.module.scss'

export default function CategorySection({ data }) {
  const { t } = useTranslation('common')

  const categories = [
    { title: 'พร้อมรับมือ โควิด-19', image: '/images/mock/home/categories/category_1.png' },
    { title: 'ร้านค้าส่ง', image: '/images/mock/home/categories/category_2.png' },
    { title: 'เฉพาะบิ๊กซี', image: '/images/mock/home/categories/category_3.png' },
    { title: 'อาหารสด แช่แข็ง ผักผลไม้', image: '/images/mock/home/categories/category_4.png' },
    { title: 'อาหารแห้ง เครื่องปรุง', image: '/images/mock/home/categories/category_5.png' },
    { title: 'เครื่องดื่ม ขนมขบเคี้ยว', image: '/images/mock/home/categories/category_6.png' },
    { title: 'สุขภาพและ ความงาม', image: '/images/mock/home/categories/category_7.png' },
    { title: 'แม่และเด็ก', image: '/images/mock/home/categories/category_8.png' },
    { title: 'ของใช้ในบ้าน', image: '/images/mock/home/categories/category_9.png' },
    {
      title: 'เครื่องใช้ไฟฟ้า อิเลกทรอนิกส์',
      image: '/images/mock/home/categories/category_10.png',
    },
    { title: 'บ้านและไลฟ์สไตล์', image: '/images/mock/home/categories/category_11.png' },
    {
      title: 'เครื่องเขียน อุปกรณ์สำนักงาน',
      image: '/images/mock/home/categories/category_12.png',
    },
    { title: 'เสื้อผ้า เครื่องประดับ', image: '/images/mock/home/categories/category_13.png' },
    { title: 'ร้านเพรียว ฟาร์มาซี', image: '/images/mock/home/categories/category_14.png' },
    { title: 'เบสิโค', image: '/images/mock/home/categories/category_15.png' },
    { title: 'มุมสัตว์เลี้ยง', image: '/images/mock/home/categories/category_17.png' },
    { title: 'สินค้านำเข้า', image: '/images/mock/home/categories/category_18.png' },
  ]

  return (
    <section className={clsx('container', styles['categories__container'])}>
      <div className={styles['header__section']}>
        <div className={styles['title']}>{data?.section_name}</div>
      </div>

      <div className={styles.row}>
        {categories?.map((category, index) => {
          return (
            <Link href="/" key={index}>
              <a className={styles.col}>
                <Image src={category?.image} width={80} height={80} />
                <div className={styles.title}>{category?.title}</div>
              </a>
            </Link>
          )
        })}
      </div>
    </section>
  )
}

CategorySection.propTypes = {
  data: PropTypes.object,
}
