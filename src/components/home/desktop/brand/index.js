import { Splide, SplideSlide } from '@splidejs/react-splide'
import Image from 'next/image'
import Link from 'next/link'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import styles from './brand.module.scss'

export default function BrandSection({ data }) {
  const { t } = useTranslation('common')

  const brands = [
    { image: '/images/mock/home/brand_1.png' },
    { image: '/images/mock/home/brand_2.png' },
    { image: '/images/mock/home/brand_3.png' },
    { image: '/images/mock/home/brand_4.png' },
    { image: '/images/mock/home/brand_5.png' },
    { image: '/images/mock/home/brand_6.png' },
    { image: '/images/mock/home/brand_7.png' },
    { image: '/images/mock/home/brand_8.png' },
    { image: '/images/mock/home/brand_9.png' },
    { image: '/images/mock/home/brand_10.png' },
    { image: '/images/mock/home/brand_1.png' },
    { image: '/images/mock/home/brand_2.png' },
    { image: '/images/mock/home/brand_3.png' },
    { image: '/images/mock/home/brand_4.png' },
    { image: '/images/mock/home/brand_5.png' },
    { image: '/images/mock/home/brand_6.png' },
    { image: '/images/mock/home/brand_7.png' },
    { image: '/images/mock/home/brand_8.png' },
    { image: '/images/mock/home/brand_9.png' },
    { image: '/images/mock/home/brand_10.png' },
    { image: '/images/mock/home/brand_1.png' },
    { image: '/images/mock/home/brand_2.png' },
    { image: '/images/mock/home/brand_3.png' },
    { image: '/images/mock/home/brand_4.png' },
    { image: '/images/mock/home/brand_5.png' },
    { image: '/images/mock/home/brand_6.png' },
    { image: '/images/mock/home/brand_7.png' },
    { image: '/images/mock/home/brand_8.png' },
    { image: '/images/mock/home/brand_9.png' },
    { image: '/images/mock/home/brand_10.png' },
  ]

  return (
    <section className={styles['brands__container']}>
      <div className="container">
        <div className={styles['header__section']}>
          <div className={styles['title']}>{data?.section_name}</div>
          <div className={styles['seemore']}>
            {t('section-seemore')}
            <i className={styles['seemore-icon']}>
              <svg viewBox="0 0 40 40">
                <path d="m15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z"></path>
              </svg>
            </i>
          </div>
        </div>

        <div className={styles.slidebar}>
          <Splide
            options={{
              perPage: 10,
            }}
          >
            {brands?.map((brand, index) => {
              return (
                <SplideSlide key={index}>
                  <Link href="/">
                    <a>
                      <Image src={brand?.image} width={104} height={104} />
                    </a>
                  </Link>
                </SplideSlide>
              )
            })}
          </Splide>
        </div>
      </div>
    </section>
  )
}

BrandSection.propTypes = {
  data: PropTypes.object,
}
