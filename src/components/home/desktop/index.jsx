import styles from './home.module.scss'

export default function HomeDesktop(props) {
  return (
    <div className={styles.container}>
      <section className={styles.home}>
        <div>Home Page</div>
      </section>
    </div>
  )
}

HomeDesktop.propTypes = {}
