import Icon from '@components/common/icon'
import ProductCard from '@components/common/product/productCard'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import { mockProducts1, mockProducts2, mockProducts3, mockProducts4 } from './mockProducts'
import styles from './productTab.module.scss'

export default function ProductTabSection({ data }) {
  const { t } = useTranslation('common')
  const [tab, setTab] = useState(0)
  const [runAnimate, setRunAnimate] = useState(false)

  function changeTab(tab) {
    setRunAnimate(true)
    setTimeout(() => {
      setTab(tab)
      setRunAnimate(false)
    }, 350)
  }

  data = [
    {
      title: 'สำหรับคุณ',
      products: mockProducts1.concat(mockProducts3).concat(mockProducts4),
    },
    { title: 'ลดราคาถูกสุดสุด', products: mockProducts2 },
    {
      title: '1 แถม 1',
      products: mockProducts3.concat(mockProducts3).concat(mockProducts3),
    },
    { title: 'ซื้อ 2 ชิ้นถูกกว่า', products: mockProducts4 },
  ]

  return (
    <section className={clsx('container', styles['tabs__container'])}>
      <div className={styles['header__section']}>
        <div>
          {data?.map(({ title }, index) => (
            <span
              className={clsx(styles['tab__title'], !runAnimate && tab === index && styles.active)}
              key={index}
              onClick={() => changeTab(index)}
            >
              {index === 0 && <Icon path="heart" w="19" h="19" />}
              {title}
              <div className={styles['current-bar']} />
            </span>
          ))}
        </div>
        <div className={styles['tab__seemore']}>
          {t('section-seemore')}
          <i className={styles['tab__seemore-icon']}>
            <svg viewBox="0 0 40 40">
              <path d="m15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z"></path>
            </svg>
          </i>
        </div>
      </div>
      <div className={clsx(styles['products__row'], runAnimate && styles['animate'])}>
        {data?.[tab]?.products?.map((product, index) => {
          return (
            <div key={`products_${index}`} className={styles['products__col']}>
              <ProductCard productDetail={product} />
            </div>
          )
        })}
      </div>
    </section>
  )
}

ProductTabSection.propTypes = {
  data: PropTypes.object,
}
