import ProductCard from '@components/common/product/productCard'
import { Splide, SplideSlide } from '@splidejs/react-splide'
import clsx from 'clsx'
import Image from 'next/image'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import { mockProducts3 } from './mockProducts'
import styles from './productAndBanner.module.scss'

export default function ProductAndBannerSection({ data }) {
  const { t } = useTranslation('common')

  const banners = [
    { image: '/images/mock/home/banner-product_1.png' },
    { image: '/images/mock/home/banner-product_1.png' },
    { image: '/images/mock/home/banner-product_1.png' },
  ]

  return (
    <section className={clsx('container', styles.container)}>
      <div className={styles['header__section']}>
        <div className={styles['title']}>{data?.section_name}</div>
        <div className={styles['seemore']}>
          {t('section-seemore')}
          <i className={styles['seemore-icon']}>
            <svg viewBox="0 0 40 40">
              <path d="m15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z"></path>
            </svg>
          </i>
        </div>
      </div>

      <div className={styles.content_section}>
        <div className={styles.banner}>
          <Splide options={{ arrows: false }}>
            {banners?.map((banner, index) => {
              return (
                <SplideSlide key={index}>
                  <Image src={banner?.image} width={384} height={256} />
                </SplideSlide>
              )
            })}
          </Splide>
        </div>

        <div className={styles.product}>
          <div className={styles.slidebar}>
            <Splide options={{ perPage: 4 }}>
              {mockProducts3
                .concat(mockProducts3)
                .concat(mockProducts3)
                ?.map((product, index) => {
                  return (
                    <SplideSlide key={index}>
                      <ProductCard productDetail={product} />
                    </SplideSlide>
                  )
                })}
            </Splide>
          </div>
        </div>
      </div>
    </section>
  )
}

ProductAndBannerSection.propTypes = {
  data: PropTypes.object,
}
