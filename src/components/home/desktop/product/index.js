import ProductSlide from '@components/common/slidebar/productSlide'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import styles from './product.module.scss'

export default function ProductSection({ data }) {
  const { t } = useTranslation('common')

  return (
    <section className={styles['product__container']}>
      <div className="container">
        <div className={styles['header__section']}>
          <div className={styles['products__title']}>{data?.section_name}</div>
          <div className={styles['products__seemore']}>
            {t('section-seemore')}
            <i className={styles['products__seemore-icon']}>
              <svg viewBox="0 0 40 40">
                <path d="m15.5 0.932-4.3 4.38 14.5 14.6-14.5 14.5 4.3 4.4 14.6-14.6 4.4-4.3-4.4-4.4-14.6-14.6z"></path>
              </svg>
            </i>
          </div>
        </div>
        <ProductSlide items={data?.products_summary?.products} />
      </div>
    </section>
  )
}

ProductSection.propTypes = {
  data: PropTypes.object,
}
