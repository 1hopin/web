export const mockProducts1 = [
  {
    product_id: 181,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/14/88/8850157400114/thumbnail/8850157400114.jpg',
    name_en: 'BP BENTO TASTY FISH 4G.P12',
    name: 'BPเบนโตะหมึกอบทรงเครื่อง4ก.P12',
    price_incl_tax: 49.5,
    final_price_incl_tax: 49.5,
    unit: 'แพ็ค',
    unit_en: 'Pack',
    slug: 'bento-seasoned-baked-squid-sweet-and-spicy-flavor-5-g-pack-12',
  },
  {
    product_id: 182,
    name: 'BPเบนโตะหมึกอบน้ำพริกไทย4ก.P12',
    price_incl_tax: 49.5,
    final_price_incl_tax: 49.5,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/44/88/8850157400244/thumbnail/8850157400244.jpg',
    slug: 'bento-crispy-squid-contains-6-8-grams-12-packs-that-its-unique-thai-chilli-sauce-taste-an-excellent-quality-squid-mixed-with-the-unique-thai-chilli-sauce-by-the-baked-foods-processes-size-6-8-g-12-packs-this-chiili-taste-unique-tasty-seleced-squid-with-th',
    name_en: 'BP BENTO CILL FISH 4G.P12',
    unit: 'ถุง',
    unit_en: 'Pack',
  },
  {
    unit_en: 'Bag',
    name: 'เลย์ร็อคเกลือ 73 กรัม',
    name_en: 'LAYS ROCK SALT 73G',
    product_id: 183,
    slug: 'lay-s-smooth-wavy-contains-75-grams-original-flavor',
    price_incl_tax: 29,
    unit: 'ถุง',
    final_price_incl_tax: 29,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/21/88/8850718801121/thumbnail/8850718801121_7.jpg',
  },
  {
    name_en: 'BP LAY STAX ORIGINAL PLUS 14G P12',
    product_id: 184,
    slug: 'lay-s-chips-lay-s-stax-original-plus-flavor-contains-14-grams-12-packs-snack-made-from-genuine-potatoes-with-sacuy-and-yummy-taste-it-is-a-unique-combination-of-great-taste-and-good-fun-this-deliciously-fresh-tasting-and-perfectly-crispy-po',
    name: 'BPเลย์สแตคส์ออริจินอล14กP12',
    unit_en: 'Pack',
    price_incl_tax: 50,
    final_price_incl_tax: 48,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/02/88/8850718707102/thumbnail/8850718707102_4.jpg',
    unit: 'แพ็ค',
  },
  {
    product_id: 185,
    slug: 'tasto-potato-had-launched-with-the-speicial-tasty-as-salt-and-sour-flavour-flat-sheets-14-grams-packs-12-added-with-salt-and-sour-flavor-to-be-good-tasty-through-the-high-process-of-production-size-14-g-packs-12-flat-sheets-roasted-squids-mixed-with-seawe',
    name: 'BP_เทสโตซอลท์&ซาวร์ 11 ก. F/C',
    unit: 'แพ็ค',
    unit_en: 'Pack',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/09/88/8851004416609/thumbnail/8851004416609.jpg',
    price_incl_tax: 50,
    final_price_incl_tax: 50,
    name_en: 'BP_TASTO SALT&SOUR 11 G. F/C',
  },
  {
    name: 'BPขนมอบกรอบคอนเน่ชีส15G.P12(M)',
    final_price_incl_tax: 50,
    price_incl_tax: 50,
    slug: 'cornae-crispy-corn-cheese-flavor-15-g-pack-12',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/19/88/8851727903219/thumbnail/8851727903219_7.jpg',
    name_en: 'Cornae Cheese Extruded 12*15 Gm(M)',
    unit: 'แพ็ค',
    product_id: 186,
    unit_en: 'Pack',
  },
  {
    name_en: 'CORNAE EXTRUDED 12*14 GM 47 BAHT(M)',
    price_incl_tax: 50,
    slug: 'cornae-crispy-corn-chips-original-flavor-14-g-pack-12',
    unit_en: 'Pack',
    final_price_incl_tax: 50,
    name: 'BPข้าวโพดอบกรอบคอนเน่14P12ดั้ง(M)',
    product_id: 187,
    unit: 'แพ็ค',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/11/88/8851727903011/thumbnail/8851727903011.jpg',
  },
  {
    slug: 'cornae-crispy-corn-contains-56-grams-cheese-flavor',
    name: 'ขนมอบกรอบคอนเน่56G(MP)',
    unit: 'ถุง',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/54/88/8851727003254/thumbnail/8851727003254_5.jpg',
    price_incl_tax: 20,
    product_id: 188,
    unit_en: 'Bag',
    final_price_incl_tax: 20,
    name_en: 'CORNAE EXTRUDED CHEESE62GM(MP)',
  },
]

export const mockProducts2 = [
  {
    unit_en: 'Pack',
    final_price_incl_tax: 233,
    unit: 'แพ็ค',
    slug: 'pumpui-smiling-fish-canned-seafood-fried-baby-clam-with-chilli-40-g-pack-10',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/59/88/8850088605459/thumbnail/8850088605459_1.jpg',
    name: 'BPปุ้มปุ้ยหอยลายทอด40กแพ็ค10',
    name_en: 'PUMPUIBABYCLAMFRIED40gP10',
    price_incl_tax: 233,
    product_id: 201,
  },
  {
    name: 'BPแมคเคอเรลทอดราดพริก155GX10',
    unit_en: 'Pack',
    final_price_incl_tax: 200,
    unit: 'แพ็ค',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/20/88/8850088601420/thumbnail/8850088601420_9.jpg',
    product_id: 205,
    name_en: 'SMILING FISH SARDI W/CHILLI 155G*10',
    price_incl_tax: 200,
    slug: 'pumpui-fried-mackerel-in-chili-sauce-155-g-pack-10',
  },
  {
    price_incl_tax: 174,
    product_id: 608,
    final_price_incl_tax: 174,
    name: 'BPโรซ่าแมคเรล(ดึง)155X10',
    name_en: 'ROZA MACKEREL IN TOMSAU155G*10',
    slug: 'roza-mackerel-in-tomato-sauce-155-g-pack-10',
    unit: 'แพ็ค',
    unit_en: 'Pack',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/84/88/8850511120184/thumbnail/8850511120184.jpg',
  },
  {
    product_id: 993,
    unit_en: 'Can',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/02/88/8850088605602/thumbnail/8850088605602_25.jpg',
    name_en: 'SMILINGFISHFRIEDBABYCLAM+CHILLI70g.(M)',
    final_price_incl_tax: 39,
    slug: 'pumpui-fried-baby-clam-with-chilli-spicy-70g',
    price_incl_tax: 39,
    name: 'หอยลายทอดรสเผ็ด70G.ปลายิ้ม(M)',
    unit: 'กระป๋อง',
  },
  {
    unit: 'แพ็ค',
    unit_en: 'Pack',
    final_price_incl_tax: 175,
    price_incl_tax: 175,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/65/88/8850468208065/thumbnail/8850468208065.jpg',
    slug: 'sealect-mackerel-can-155g-pack-10',
    name: 'BPซีเล็คปลาแมคเคอเรล155Gแพค10',
    name_en: 'BP SEALECT MACKEREL IN TOM SAU 155G*10 ',
    product_id: 994,
  },
]

export const mockProducts3 = [
  {
    name: 'เดทตอลHWออริจินัลปั้ม225มล',
    name_en: 'DETTOL HW ORIGINAL PUM 225ML',
    price_incl_tax: 85,
    product_id: 283,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/67/88/8850360025067/thumbnail/8850360025067_1.jpg',
    final_price_incl_tax: 85,
    slug: 'dettol-hand-wash-250-g',
    unit: 'ขวด',
    unit_en: 'Bottle',
  },
  {
    product_id: 284,
    name_en: 'KIREI HAND FOAM 200ML. REFILL',
    name: 'คิเรอิโฟมล้างมือ200 มล.ถุง',
    price_incl_tax: 35,
    final_price_incl_tax: 35,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/45/88/8850002020245/thumbnail/8850002020245.jpg',
    unit: 'ถุง',
    unit_en: 'Bag',
    slug: 'kirei-kirei-foaming-hand-wash-200ml',
  },
  {
    slug: 'protex-soap-icy-cool-75-ml-pack-4',
    unit: 'แพ็ค',
    price_incl_tax: 59,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/59/88/8850006534359/thumbnail/8850006534359_4.jpg',
    name: 'โพรเทคสบู่ไอซ์ซี่คูล65กรัมx4',
    final_price_incl_tax: 59,
    unit_en: 'Pack',
    product_id: 958,
    name_en: 'PROTEX ICY65Gx4',
  },
  {
    name_en: 'PARROT BAR 4X 70G PINK',
    final_price_incl_tax: 37,
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/43/88/8851929006343/thumbnail/8851929006343_6.jpg',
    product_id: 959,
    slug: 'parrot-botanicals-soap-pink-bar-75-g-pack-4',
    name: 'สบู่พฤกษานกแก้ว4X70ก.ชมพู',
    unit: 'แพ็ค',
    unit_en: 'Pack',
    price_incl_tax: 45,
  },
]

export const mockProducts4 = [
  {
    slug: 'knock-down-cabinet-40-x-42-x-68-cm-3-stories',
    unit_en: 'Piece',
    price_incl_tax: 399,
    name: 'FLD ตู้น็อคดาวน์ 3 ชั้น(40X42X68ซม)',
    name_en: 'FLD KNOCK DOWN 3L 40X42X68CM',
    final_price_incl_tax: 299,
    product_id: 315,
    unit: 'ชิ้น',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/27/88/8851370700227/thumbnail/8851370700227_1.jpg',
  },
  {
    product_id: 319,
    name: 'FLD กล่องเอนกประสงค์80LT สีทึบ',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/25/88/8851370966425/thumbnail/8851370966425_1_1.jpg',
    price_incl_tax: 299,
    final_price_incl_tax: 199,
    slug: 'multipurpose-opaque-storage-box-capacity-80-l-assorted-color',
    name_en: 'FLD MULTIPURPOSE BOX SOLID COLOR 80LT',
    unit_en: 'Piece',
    unit: 'ใบ',
  },
  {
    price_incl_tax: 130,
    name_en: 'แก๊สกระป๋องแพ็ค 3 ALTA',
    slug: 'alta-gas-butane-pack-3',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/82/88/8851854411182/thumbnail/8851854411182_4.jpg',
    product_id: 331,
    unit_en: 'Pack',
    final_price_incl_tax: 119,
    name: 'แก๊สกระป๋องแพ็ค 3 ALTA',
    unit: 'แพ็ค',
  },
  {
    unit_en: 'Piece',
    final_price_incl_tax: 6990,
    name: 'MAGNETIC BIKE',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/16/88/8851854233616/thumbnail/8851854233616_7.jpg',
    slug: 'pink-color-55-x-80-5-x-124-cc',
    name_en: 'MAGNETIC BIKE',
    product_id: 347,
    unit: 'ชิ้น',
    price_incl_tax: 6990,
  },
  {
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/97/88/8855951002397/thumbnail/8855951002397_8.jpg',
    unit_en: 'Piece',
    name: '*BIKE 16" SELECTED',
    price_incl_tax: 4790,
    final_price_incl_tax: 4790,
    product_id: 348,
    unit: 'คัน',
    slug: 'select-16-inch-wheels',
    name_en: '*BIKE 16" SELECTED',
  },
  {
    final_price_incl_tax: 2990,
    name_en: '*BIKE 24" TURBO EXCEL',
    product_id: 350,
    slug: 'excel-24-inch-wheels',
    unit_en: 'Piece',
    name: '*BIKE 24" TURBO EXCEL',
    price_incl_tax: 5190,
    unit: 'คัน',
    thumbnail_image:
      'https://nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com/public/media/catalog/product/77/88/8855951001277/thumbnail/8855951001277_6.jpg',
  },
]
