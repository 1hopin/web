import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import styles from './error.module.scss'

function ErrorMobile(props) {
  const { statusCode } = props
  const { t } = useTranslation('common')
  const errorTextSlug = `error-${statusCode}`
  return (
    <div className={'container'}>
      <div className={`container__row ${styles.error}`}>
        <div className={`container__col-12`}>
          <div className={styles['error__card']}>
            <div className={styles['error__card--item']}>{t(errorTextSlug)}</div>
          </div>
        </div>
      </div>
    </div>
  )
}

ErrorMobile.propTypes = {
  statusCode: PropTypes.number,
}

export default ErrorMobile
