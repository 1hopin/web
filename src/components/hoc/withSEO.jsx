/* eslint-disable react/prop-types */
import Head from 'next/head'
import { useRouter } from 'next/router'

export default function withSEO(BaseComponent) {
  function WrappedComponent(props) {
    const seoData = props?.pageProps?.seoData || {}
    const router = useRouter()
    const currentPath = router?.asPath || '/'
    const appDomain = process.env.HOST
    const pathUrl = `${appDomain}${currentPath}`

    const template = seoData?.dynamicMeta || {}
    function getDataWithArgument(keyword) {
      let data = template[keyword]
      const passingArgumentsData = seoData
      if (data && passingArgumentsData) {
        if (data.search(/{{.*}}/g) > -1) {
          Object.keys(passingArgumentsData).forEach((key) => {
            const regex = new RegExp(`{{${key}}}`, 'g')
            data = data.replace(regex, passingArgumentsData[key], '')
          })
        }
      }
      return data
    }

    const metaTitle = getDataWithArgument('title')
    const metaDescription = getDataWithArgument('description')
    const metaKeywords = getDataWithArgument('keywords')

    // meta of content type
    const metaContents = [
      { name: 'description', content: metaDescription },
      { name: 'keywords', content: metaKeywords },
      { name: 'twitter:title', content: metaTitle },
      { name: 'twitter:description', content: metaDescription },
      { name: 'twitter:image', content: seoData?.image },
    ]

    // meta of properties type
    const metaProperties = [
      { name: 'og:type', content: 'website' },
      { name: 'og:image', content: seoData?.image },
      { name: 'og:url', content: pathUrl },
      { name: 'og:title', content: metaTitle },
      { name: 'og:description', content: metaDescription },
    ]

    return (
      <>
        {seoData && (
          <Head>
            <title>{metaTitle}</title>
            {metaContents.map((val, index) => {
              const content = val?.content || ''
              if (content !== '') {
                return (
                  <meta key={`meta_content_${index}`} name={val?.name || ''} content={content} />
                )
              }
            })}
            {metaProperties.map((val, index) => {
              const content = val?.content || ''
              if (content !== '') {
                return (
                  <meta key={`meta_properties_${index}`} property={val.name} content={content} />
                )
              }
            })}
          </Head>
        )}
        <BaseComponent {...props} />
      </>
    )
  }

  WrappedComponent.getInitialProps = async ({ ctx }) => {
    const props = await BaseComponent.getInitialProps({ ctx })
    return props
  }
  return WrappedComponent
}
