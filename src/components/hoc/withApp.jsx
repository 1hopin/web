import languages from '@configs/languages'
import { getI18nData } from '@libs/i18n/_helper'
import { getUserAgent } from '@utils/userAgent'

import withSchemas from './withSchemas'
import withSEO from './withSEO'

export default function withApp(AppComponent) {
  AppComponent.getInitialProps = async ({ ctx }) => {

      if (!ctx.req.cookies?.authToken && ctx.req.path !== '/member/login' && ctx.req.path !== '/member/register') {
        ctx.res.writeHead(307, { Location: '/member/login' })
        ctx.res.end()
      } else if (ctx.req.path === '/member/login' && !!ctx.req.cookies?.authToken) {
        ctx.res.writeHead(307, { Location: '/' })
        ctx.res.end()
      }

    

    return {
      pageProps: {
        statusCode: ctx.res.statusCode,
        authDataFromServer: ctx.req.cookies?.profile,
        ...(ctx.res.statusCode !== 200 ? getPropsErrorPage(ctx) : null),
      },
    }
  }

  return withSEO(withSchemas(AppComponent))
}

function getPropsErrorPage(ctx) {
  let language = ctx.req.cookies.language
  if (!languages.includes(language)) language = 'th'
  ctx.req.language = language

  return {
    userAgent: getUserAgent(ctx.req),
    language,
    ...getI18nData(ctx),
  }
}
