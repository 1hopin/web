/* eslint-disable react/prop-types */
import { withRouter } from 'next/router'
import { useEffect, useState } from 'react'

const withFetchMore =
  (options = {}, callAPI) =>
  (WrappedComponent) => {
    function ComponentWithFetchMore(props) {
      const [data, setData] = useState([])
      const [pagination, setPagination] = useState(1)
      const [lastPagination, setLastPagination] = useState(2)
      const [isLoading, setIsLoading] = useState(false)
      const [isFinished, setIsFinished] = useState(false)
      const { limit, offset } = options
      const { router } = props

      useEffect(() => {
        setData([])
        setIsLoading(false)
        setIsFinished(false)
      }, [router.asPath])

      const fetchMore = () => {
        const extraparams = {
          limit,
          pagination: pagination,
        }

        setIsLoading(true)

        callAPI({ ...extraparams }).then((newData) => {
          const lastIndex = newData?.data?.last_page || 1
          setLastPagination(lastIndex)

          setIsLoading(false)
          setPagination(pagination + offset)

          if (pagination >= lastPagination) {
            setIsFinished(true)
            setIsLoading(false)
          }

          setData(data.concat(newData?.data?.products || []))
        })
      }

      return (
        <WrappedComponent
          {...props}
          isLoading={isLoading}
          isFinished={isFinished}
          fetchMore={fetchMore}
          data={data}
        />
      )
    }

    return withRouter(ComponentWithFetchMore)
  }

export default withFetchMore
