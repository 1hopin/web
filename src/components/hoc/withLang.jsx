import languages from '@configs/languages'

export default function withLang(getServerSideProps) {
  return async (context) => {
    let language = context.req.cookies.language
    if (!languages.includes(language)) language = 'th'
    context.req.language = language

    return {
      props: {
        ...(await getServerSideProps(context)).props,
        language,
      },
    }
  }
}
