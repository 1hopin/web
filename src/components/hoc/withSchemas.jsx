/* eslint-disable react/prop-types */
import Head from 'next/head'

export default function withSchemas(BaseComponent) {
  function WrappedComponent(props) {
    const breadcrumb = props?.pageProps?.breadcrumbSchemas

    const getUrl = ({ routeName, params }) => {
      const domain = process.env.HOST
      let fullUrl = domain
      if (routeName) {
        fullUrl = `${fullUrl}/${routeName}`
      }

      if (params) {
        const id = params?.id || ''
        fullUrl = `${fullUrl}/${id}`
      }

      return fullUrl
    }

    let schemasBreadcrumb = {}
    if (breadcrumb) {
      const breadcrumbElements = breadcrumb.map(([label, routeName, params], index) => {
        const fullUrl = getUrl({ routeName, params })
        return {
          '@type': 'ListItem',
          position: index + 1,
          name: label.name,
          item: fullUrl,
        }
      })

      schemasBreadcrumb = {
        '@context': 'https://schema.org',
        '@type': 'BreadcrumbList',
        itemListElement: breadcrumbElements,
      }
    }

    // console.log('schemasBreadcrumb > ', schemasBreadcrumb)
    return (
      <>
        {breadcrumb && (
          <Head>
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{ __html: JSON.stringify(schemasBreadcrumb) }}
            />
          </Head>
        )}
        <BaseComponent {...props} />
      </>
    )
  }

  WrappedComponent.getInitialProps = async ({ ctx }) => {
    const props = await BaseComponent.getInitialProps({ ctx })
    return props
  }
  return WrappedComponent
}
