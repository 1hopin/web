import { getI18nData } from '@libs/i18n/_helper'

export function withTranslation(pageNamespaces) {
  return (getServerSideProps) => async (context) => {
    return {
      props: {
        ...(await getServerSideProps(context)).props,
        ...getI18nData(context, pageNamespaces),
      },
    }
  }
}
