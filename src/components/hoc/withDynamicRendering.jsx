import compose from 'lodash/flowRight'

import withLang from './withLang'
import { withTranslation } from './withTranslation'
import withUA from './withUA'

// withPage
export default function withDynamicRendering(options = {}) {
  return function (Component) {
    const hocs = [withUA, withLang, withTranslation(options.ns)]

    return compose(...hocs)(Component)
  }
}
