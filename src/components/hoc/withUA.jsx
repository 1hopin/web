import { getUserAgent } from '@utils/userAgent'

export default function withUA(getServerSideProps) {
  return async (context) => {
    context.req.ua = getUserAgent(context.req)
    return {
      props: {
        ...(await getServerSideProps(context)).props,
        userAgent: context.req.ua,
      },
    }
  }
}
