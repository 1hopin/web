import Icon from '@components/common/icon'
import clsx from 'clsx'
import Link from 'next/link'
import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'

import styles from './footer.module.scss'

export default function MobileFooter() {
  const { t } = useTranslation('common')
  const [scrollDir, setScrollDir] = useState()

  useEffect(() => {
    let lastScrollY = window.pageYOffset

    const onScroll = () => {
      const scrollY = window.pageYOffset
      setScrollDir(scrollY > lastScrollY ? 'down' : 'up')
      lastScrollY = scrollY > 0 ? scrollY : 0
    }

    window.addEventListener('scroll', onScroll)
    return () => window.removeEventListener('scroll', onScroll)
  }, [scrollDir])

  return (
    <footer className={clsx(styles.footer, scrollDir === 'down' && styles.hide)}>
      <Link href="/">
        <div>
          <div className={styles['icon-wrap']}>
            <Icon path="home" w="24" h="24" strokeWidth="0.5" />
          </div>
          <div className={styles.title}>{t('footer-mobile-menu1')}</div>
        </div>
      </Link>
      <div>
        <div className={styles['icon-wrap']}>
          <Icon path="list-dot" w="24" h="24" strokeWidth="0.7" />
        </div>
        <div className={styles.title}>{t('footer-mobile-menu2')}</div>
      </div>
      <div>
        <div className={styles['icon-wrap']}>
          <Icon path="list-dot" w="24" h="24" strokeWidth="0.4" />
        </div>
        <div className={styles.title}>{t('footer-mobile-menu3')}</div>
      </div>
      <Link href="/member">
        <div>
          <div className={styles['icon-wrap']}>
            <Icon path="user-inactive" w="24" h="24" strokeWidth="0.5" gw="25" gh="25" />
          </div>
          <div className={styles.title}>{t('footer-mobile-menu4')}</div>
        </div>
      </Link>
    </footer>
  )
}
