import Image from 'next/image'

import styles from './header.module.scss'
// import HeaderUser from './user'
import LangSwitcher from './langSwitcher'

export default function MobileHeader() {
  return (
    <header className={styles['header']}>
      <div className={styles['container-header']}>
        <div className={styles['wrapper-menu']}>
          <div className={styles['menu-mobile']}>
            <Image src="/images/icon/hamburger-menu.svg" width={36} height={36} />
            {/* <HeaderUser /> */}
            <LangSwitcher />
          </div>
        </div>
      </div>
    </header>
  )
}
