import Icon from '@components/common/icon'
import { useTranslation } from 'react-i18next'

import styles from './user.module.scss'

export default function HeaderUser() {
  const { t } = useTranslation('common')

  return (
    <div className={styles['wrapper_user']}>
      <div className={styles['section__login']}>
        <Icon path="user-inactive" w="20" h="20" strokeWidth="0.3" gw="24" gh="24" />
        <div className={styles['wrapper__msg-login']}>
          {t('header-mainmenu-signIn')}
          <span>{t('header-mainmenu-or')}</span>
          <div>{t('header-mainmenu-signUp')}</div>
        </div>
      </div>
    </div>
  )
}
