import { useRouter } from 'next/router'
import PropTypes from 'prop-types'

import Footer from './footer'
import Header from './header'

export default function MobileLayout(props) {
  const { children } = props
  const router = useRouter()
  const isHideHeaderFooter = ['/hello'].indexOf(router?.asPath) > -1

  return (
    <>
      {/* {!isHideHeaderFooter && <Header />} */}
      <div className='h-100'>
      {/* <div class="fixed-background"></div> */}
      <main>{children}</main>
      </div>
      {/* {!isHideHeaderFooter && <Footer />} */}
    </>
  )
}

MobileLayout.propTypes = {
  children: PropTypes.node.isRequired,
  props: PropTypes.object,
}
