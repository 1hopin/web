import Icon from '@components/common/icon'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

import styles from './header.module.scss'
import LangSwitcher from './langSwitcher'
import UserMenu from './userMenu'

export default function Header() {
  const { t } = useTranslation('common')
  return (
    <div className={styles.header}>
      <section>
        <div className="container">
          <div className={styles['header__left']}>{/* something */}</div>

          <div className={styles['header__right']}>
            <div>{t('header-topmenu-download')}</div>
            <div>
              <Icon path="help" w="17" h="17" strokeWidth="0.2" />
              {t('header-topmenu-help')}
            </div>
            <LangSwitcher />
          </div>
        </div>
      </section>

      <section>
        <div className="container">
          <UserMenu />
        </div>
      </section>
    </div>
  )
}

Header.propTypes = {
  props: PropTypes.object,
}
