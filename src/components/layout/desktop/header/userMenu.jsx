import Icon from '@components/common/icon'
import { ModalContext } from '@components/providers/modal'
import { displayFullname, useAuth } from '@utils/helper/member/auth'
import Link from 'next/link'
import { useContext } from 'react'
import { useTranslation } from 'react-i18next'

import styles from './header.module.scss'

export default function UserMenu() {
  const { t } = useTranslation('common')
  const { user, isAuthenticated, logout } = useAuth()
  const { openModal } = useContext(ModalContext)

  return (
    <>
      {!isAuthenticated ? (
        <div className={styles['btn-sign']} 
        // onClick={() => openModal('login')}
        >
          {/* <Icon
            path="user"
            w="28"
            h="28"
            gw="28"
            gh="28"
            strokeWidth="0.5"
            className={styles['icon-user']}
          />
          {t('header-mainmenu-signIn')}
          <span>{t('header-mainmenu-or')}</span>
          {t('header-mainmenu-signUp')} */}
        </div>
      ) : (
        <div className={styles['user__dropdown']}>
          <div className={styles['user__dropdown-active']}>
            <div className={styles['user-image']}>
              <img src="/images/mock/user-profile.png" className={styles['icon-logined']} alt="" />
            </div>
            <div className={styles['message-box']}>
              <p className={styles['name']}>{displayFullname(user, 15)}</p>
              <div className={styles['big-card']}>
                <img src="/images/icon/icon-big-card.png" className={styles['icon']} alt="" />
                <p className={styles['subject']}>12,000 คะแนน</p>
              </div>
            </div>
            <Icon
              path="chevron-down"
              w="11"
              h="11"
              strokeWidth="3"
              className={styles['user__dropdown-active-icon']}
            />
          </div>
          <div className={styles['user__dropdown-content']}>
            <Link href="/member/update-profile">
              <span>Profile</span>
            </Link>
            <span onClick={logout}>Logout</span>
          </div>
        </div>
      )}
    </>
  )
}
