import Icon from '@components/common/icon'
import { changeLanguage } from '@libs/i18n/_helper'
import { useTranslation } from 'react-i18next'

import styles from './langSwitcher.module.scss'

export function LangSwitcher() {
  const { i18n } = useTranslation('common')
  const toggleLang = () => changeLanguage(i18n.language === 'th' ? 'en' : 'th')

  return (
    <div className={styles.language}>
      <div className={styles['language-menu']}>
        <span>{i18n.language === 'en' ? 'Eng' : 'ไทย'}</span>
        <Icon path="chevron-down" w="11" h="11" strokeWidth="3" className={styles['icon-down']} />
      </div>
      <div className={styles['language-dropdown']}>
        <span className={styles.current}>{i18n.language === 'en' ? 'Eng' : 'ไทย'}</span>
        <span onClick={toggleLang}>{i18n.language === 'en' ? 'ไทย' : 'Eng'}</span>
      </div>
    </div>
  )
}

export default LangSwitcher
