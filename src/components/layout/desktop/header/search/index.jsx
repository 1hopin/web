import Icon from '@components/common/icon'
import { ToastContext } from '@components/providers/toast'
import PropTypes from 'prop-types'
import { useContext } from 'react'
import { useTranslation } from 'react-i18next'

import styles from './search.module.scss'

export default function Search() {
  const { t } = useTranslation('common')
  const { toast } = useContext(ToastContext)

  return (
    <div className={styles['search-wrap']}>
      <div className={styles['search-wrap-input-wrap']}>
        <input
          type="search"
          autoComplete="off"
          placeholder={t('header-mainmenu-search-placeholder')}
        />
        <button title="search" onClick={() => toast('Click Search')}>
          <Icon path="search" w="19" h="19" />
        </button>
      </div>
    </div>
  )
}

Search.propTypes = {
  props: PropTypes.object,
}
