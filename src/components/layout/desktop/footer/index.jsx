import { useTranslation } from 'react-i18next'

import styles from './footer.module.scss'

export default function Footer() {
  const { t } = useTranslation('common')

  return (
    <footer>
      <div className={styles.information}>
        <div className="container"></div>
      </div>
    </footer>
  )
}
