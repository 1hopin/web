import { useDevice } from '@utils/userAgent'
import dynamic from 'next/dynamic'

const DesktopLayout = dynamic(() => import('./desktop'))
const MobileLayout = dynamic(() => import('./mobile'))

export default function Layout(props) {
  const { isMobile } = useDevice()
  return !isMobile ? <DesktopLayout {...props} /> : <MobileLayout {...props} />
}
