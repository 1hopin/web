import PropTypes from 'prop-types'
import Modal from 'react-modal'

import defaultStyles from './styles/defaultStyles'

const customStyles = {
  overlay: { ...defaultStyles.overlay },
  content: {
    ...defaultStyles.content,
    maxWidth: '500px',
    maxHeight: '250px',
  },
}

export default function FeedbackModal(props) {
  return (
    <Modal
      isOpen={props.isOpen}
      style={customStyles}
      onRequestClose={props.closeModal}
      ariaHideApp={false}
    >
      <h2 style={{ marginTop: 100, textAlign: 'center', fontSize: 35 }}>Feedback</h2>
    </Modal>
  )
}

FeedbackModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
}
