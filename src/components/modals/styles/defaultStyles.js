export default {
  overlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    zIndex: '99',
  },
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '100%',
    height: '100%',
    padding: 0,
    border: 'none',
    borderRadius: 0,
    backgroundColor: '#eee',
  },
}
