import Icon from '@components/common/icon'
import { apiSendOtp, apiVerifyOtp } from '@features/api/otp'
import { numberZeroPrefix } from '@utils/helper/member/auth'
import { authEncrypt } from '@utils/helper/member/auth'
import otpCountDown from '@utils/helper/otp/otpCountDown'
import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import OtpInput from 'react-otp-input'

import styles from '../styles/login/stepVerifyOTP.module.scss'

export default function StepVerifyOTP({ changePage, data }) {
  const { t, i18n } = useTranslation('common')
  const [otp, setOTP] = useState()
  const [isDisable, setIsDisable] = useState(true)
  const [otpData, setOtpData] = useState()
  const [count, setCount] = useState(0)
  const [countInTimeout, setCountInTimeout] = useState(0)
  const [countMinutes, countSeconds] = otpCountDown()
  const [errorMessage, setErrorMessage] = useState('')
  const [otpResponseCode, setOtpResponseCode] = useState(null)

  const sendOtp = async (phoneNumber) => {
    const response = await apiSendOtp(phoneNumber, i18n.language)
    setOtpResponseCode(response?.code)
    if (response?.code === 'success') {
      setOTP('')
      setOtpData(response?.data)
    } else {
      setErrorMessage(response?.message)
    }
  }
  useEffect(() => {
    const fetchData = async () => {
      await sendOtp(data?.phoneNumber)
    }
    fetchData()
  }, [])

  const handleChange = (otp) => {
    setOTP(otp)
    if (otpResponseCode !== 'success' || otp.length < 6) {
      setIsDisable(true)
    } else {
      setIsDisable(false)
    }
  }

  const handleRequestOtp = async (otp) => {
    await sendOtp(data?.phoneNumber)
  }

  const onSubmit = async () => {
    if (otpData) {
      const response = await apiVerifyOtp(
        {
          phone_no: otpData.phone_no,
          refer: otpData.reference,
          session_id: otpData.session_id,
          otp: parseInt(otp),
        },
        i18n.language,
      )
      if (response?.code !== 'success') {
        setErrorMessage(response?.message)
      } else {
        authEncrypt(JSON.stringify(response.data))

        if (data?.action === 'forgotPassword') {
          changePage({
            page: 'forgotSetNewPassword',
            data: {
              auth: response.data.auth,
              action: data?.action,
            },
          })
        } else {
          changePage({
            page: 'registerSetNewPassword',
            data: {
              auth: response.data.auth,
              action: data?.action,
            },
          })
        }
      }
    }
  }

  const countTimer = () => {
    return (
      <span>
        ({numberZeroPrefix(countMinutes)}:{numberZeroPrefix(countSeconds)} นาที)
      </span>
    )
  }

  return (
    <>
      <div className={styles.middle}>
        <div className={styles.subtitle}>{t('otp-title')}</div>
        <div className={styles['phone-number-txt']}>
          XXX-XXX-{data?.phoneNumber.substring(data?.phoneNumber.length - 4)}
        </div>
        {otpData ? <div className={styles['otp-ref-txt']}>Ref: {otpData?.reference}</div> : ''}
        <OtpInput
          value={otp}
          onChange={handleChange}
          numInputs={6}
          isInputNum
          shouldAutoFocus
          containerStyle={styles['otp-number-wrapper']}
          inputStyle={`${styles['otp-number']} ${errorMessage ? styles['otp-number-error'] : ''}`}
          focusStyle={styles['otp-number-focus']}
        />
        {errorMessage ? <div className={styles['error-message']}>{errorMessage}</div> : ''}

        <div className={styles['otp-resend-txt']} onClick={handleRequestOtp}>
          <Icon path="rotate" w="18" h="18" strokeWidth="0.5" />
          {t('otp-re-send')}
          {!errorMessage ? countTimer() : ''}
        </div>
      </div>
      <button className={styles.next} disabled={isDisable} onClick={onSubmit}>
        {t('button-confirm')}
      </button>
    </>
  )
}

StepVerifyOTP.propTypes = {
  changePage: PropTypes.func,
  data: PropTypes.object,
}
