import { apiPhoneExisting } from '@features/api/member'
import PropTypes from 'prop-types'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import styles from '../styles/login/stepLogin.module.scss'

export default function StepLogin({ changePage }) {
  const { t, i18n } = useTranslation('common')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [isDisable, setIsDisable] = useState(true)
  const [isActive, setIsActive] = useState(true)
  const [validPhoneNumber, setValidPhoneNumber] = useState(true)

  const handleChange = (e) => {
    const phoneNumber = e.target.value.replace(/[^\d.]/gi, '')
    setPhoneNumber(phoneNumber)
    setIsDisable(phoneNumber.length !== 10)
  }

  const handleSubmit = async () => {
    if (phoneNumber.length === 10 && phoneNumber.substring(0, 1) !== '0') {
      setValidPhoneNumber(false)
      return false
    } else {
      setValidPhoneNumber(true)
    }

    const response = await apiPhoneExisting(phoneNumber)
    const data = {
      phoneNumber: phoneNumber,
      action: response?.code === 'success' ? 'existed' : 'newAccount',
    }
    if (response?.code === 'success') {
      changePage({ page: 'loginEnterPassword', data })
    } else {
      //--Register for new phone number
      changePage({ page: 'verifyOTP', data })
    }
  }

  return (
    <>
      <div className={styles.middle}>
        <div className={styles.subtiitle}>{t('auth-login-title')}</div>
        <div className={styles.desc}>{t('auth-login-desc')}</div>

        <p className={styles['text-field']}>
          <input
            type="text"
            className={!validPhoneNumber ? styles['field-error'] : ''}
            maxLength={10}
            value={phoneNumber}
            onChange={handleChange}
            required
          />
          <span className={styles['floating-label']}>{t('auth-login-placeholder')}</span>
        </p>
        <span
          className={`${styles['phone-example']} ${!validPhoneNumber ? styles['error-phone'] : ''}`}
        >
          <p>{t('auth-login-example')}</p>
          <p>{!validPhoneNumber ? t('auth-invalid-phone-number') : ''}</p>
        </span>
      </div>
      <button className={styles.next} disabled={isDisable} onClick={handleSubmit}>
        {t('button-next')}
      </button>
    </>
  )
}

StepLogin.propTypes = {
  changePage: PropTypes.func,
}
