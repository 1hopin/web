import { apiPhoneExisting } from '@features/api/member'
import Image from 'next/image'
import PropTypes from 'prop-types'
import { useState } from 'react'

import styles from '../styles/login/stepForgotPassword.module.scss'

export default function StepForgotPassword({ changePage }) {
  const [phoneNumber, setPhoneNumber] = useState('')
  const [isDisable, setIsDisable] = useState(true)
  const [errorMessage, setErrorMessage] = useState('')

  const handleChange = (e) => {
    const phoneNumber = e.target.value.replace(/[^\d.]/gi, '')
    setPhoneNumber(phoneNumber)
    setIsDisable(phoneNumber.length !== 10)
  }

  const handleSubmit = async () => {
    const response = await apiPhoneExisting(phoneNumber)
    if (response?.code === 'not_found') {
      setErrorMessage('ไม่พบเบอร์มือถือนี้ในระบบ กรุณาตรวจสอบ')
    } else {
      changePage({
        page: 'verifyOTP',
        data: {
          phoneNumber,
          action: 'forgotPassword',
        },
      })
    }
  }

  return (
    <>
      <div className={styles.middle}>
        <Image
          src="/images/member/forgot_password.png"
          width={234}
          height={234}
          className={styles.img}
        />
        <div className={styles.subtiitle}>
          <b>กรอกเบอร์มือถือ</b> ที่คุณลงทะเบียนไว้
        </div>
        <div className={`${styles['text-field']} ${errorMessage ? styles['error'] : ''}`}>
          <div>
            <input
              type="text"
              maxLength={10}
              value={phoneNumber}
              onChange={handleChange}
              required
            />
            <span className={styles['floating-label']}>ระบุเบอร์โทรศัพท์มือถือ</span>
          </div>
          <span className={`${styles['text-help']}`}>
            <p>ตัวอย่าง: 089XXXXXXX</p>
            {errorMessage ? <p className={styles['text-error']}>{errorMessage}</p> : ''}
          </span>
        </div>
      </div>

      <button className={styles.next} disabled={isDisable} onClick={handleSubmit}>
        ต่อไป
      </button>
    </>
  )
}

StepForgotPassword.propTypes = {
  changePage: PropTypes.func,
}
