import Icon from '@components/common/icon'
import { apiLoginWithPhoneNumber } from '@features/api/member'
import { authEncrypt, useAuth } from '@utils/helper/member/auth'
import PropTypes from 'prop-types'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import styles from '../styles/login/stepLoginEnterPassword.module.scss'

export default function StepLoginEnterPassword({ changePage, data }) {
  const { t } = useTranslation('common')
  const [values, setValues] = useState({
    password: '',
    showPassword: false,
    verify: { pass: false },
  })
  const phoneNumber = data?.phoneNumber
  const { setUser } = useAuth()
  const [errorMessage, setErrorMessage] = useState('')

  const handleChangePassword = (e) => {
    setValues({
      ...values,
      password: e.target.value,
      verify: { pass: e.target.value.length >= 6 },
    })
  }

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    })
  }

  const onSubmit = async () => {
    const response = await apiLoginWithPhoneNumber({
      data: {
        username: phoneNumber,
        password: values.password,
        type: 'phone',
      },
    })
    if (response?.code === 'success') {
      setErrorMessage('')
      authEncrypt(JSON.stringify(response.data))
      setUser(response.data)
      changePage({ page: 'close' })
    } else {
      setErrorMessage(t('auth-invalid-password'))
    }
  }

  return (
    <>
      <div className={styles.middle}>
        <div className={styles.subtiitle}>
          <b>{t('auth-enter-password')}</b> {t('auth-enter-password-for-login')}
        </div>
        <div className={`${styles['text-field']} ${errorMessage ? styles['error'] : ''}`}>
          <div>
            <input
              type={values.showPassword ? 'text' : 'password'}
              value={values.password}
              onChange={handleChangePassword}
              required
            />
            <span className={styles['floating-label']}>{t('auth-enter-password-placeholder')}</span>
            <button onClick={handleClickShowPassword}>
              <Icon
                path={values.showPassword ? 'eye' : 'eye-off'}
                w="20"
                h="20"
                strokeWidth="0.1"
              />
            </button>
          </div>
        </div>
        {errorMessage && (
          <div className={`${styles['text-help']} ${errorMessage ? styles['error'] : ''}`}>
            {errorMessage}
          </div>
        )}

        <div className={styles['forget-txt']}>
          <span
            onClick={() =>
              changePage({ page: 'forgotPassword', data: { phoneNumber: data?.phoneNumber } })
            }
          >
            {t('auth-button-forgot-password')}
          </span>
        </div>
      </div>

      <button className={styles.next} disabled={!values.verify.pass} onClick={onSubmit}>
        {t('button-login')}
      </button>
    </>
  )
}

StepLoginEnterPassword.propTypes = {
  changePage: PropTypes.func,
  data: PropTypes.object,
}
