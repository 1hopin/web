import clsx from 'clsx'
import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import Modal from 'react-modal'

import defaultStyles from '../styles/defaultStyles'
import styles from '../styles/login/index.module.scss'

const customStyles = {
  overlay: { ...defaultStyles.overlay },
  content: {
    display: 'flex',
    flexDirection: 'column',
    inset: 'auto 0 auto auto',
    width: '512px',
    height: '100%',
    padding: 0,
    border: 'none',
    borderRadius: 0,
    backgroundColor: '#f5f5f5',
    animation: 'slideLeftAnimation 0.5s',
  },
}

const DynamicStepLogin = dynamic(() => import('./stepLogin'), { ssr: false })
//--Exsiting Account - Enter password
const DynamicStepLoginEnterPassword = dynamic(() => import('./stepLoginEnterPassword'), {
  ssr: false,
})
//--New phone number
const DynamicStepVerifyOTP = dynamic(() => import('./stepVerifyOTP'), {
  ssr: false,
})
const DynamicStepRegisterSetNewPassword = dynamic(() => import('./stepRegisterSetNewPassword'), {
  ssr: false,
})
const DynamicStepRegisterSuccess = dynamic(() => import('./stepRegisterSuccess'), {
  ssr: false,
})
//--Forgot password
const DynamicStepForgotPassword = dynamic(() => import('./stepForgotPassword'), { ssr: false })
const DynamicStepForgotSetNewPassword = dynamic(() => import('./stepForgotSetNewPassword'), {
  ssr: false,
})

// const DynamicStepRegisterSuccess = dynamic(() => import('./stepRegisterSuccess'), { ssr: false })

// const DynamicStepSetNewPassword = dynamic(() => import('./stepSetNewPassword'), { ssr: false })

export default function RegisterModal(props) {
  const [isActive, setIsActive] = useState(true)
  const [page, setPage] = useState({ step: 'login' })
  const { t } = useTranslation('common')

  function changePage({ page, data }) {
    if (page === 'close') {
      props.closeModal()
      return
    }

    setIsActive(false)
    setTimeout(() => {
      setPage({ step: page, data })
      setIsActive(true)
    }, 500)
  }

  const pageData = {
    login: { title: t('auth-header-login') },
    loginEnterPassword: { title: t('auth-header-enter-password'), backPage: 'login' },
    enterPassword: { title: t('auth-header-enter-password'), backPage: 'login' },
    verifyOTP: { title: t('auth-header-verify-otp'), backPage: 'login' },
    registerSetPassword: { title: t('auth-header-register-set-password'), backPage: 'login' },
    registerSuccess: { title: 'Register Success' },
    forgotPassword: { title: t('auth-header-forgot-register'), backPage: 'enterPassword' },
    setNewPassword: { title: t('auth-header-set-new-password'), backPage: 'forgotPassword' },
  }[page.step]

  const DynamicComponent = {
    login: DynamicStepLogin,
    loginEnterPassword: DynamicStepLoginEnterPassword,
    verifyOTP: DynamicStepVerifyOTP,
    registerSetNewPassword: DynamicStepRegisterSetNewPassword,
    registerSuccess: DynamicStepRegisterSuccess,
    forgotPassword: DynamicStepForgotPassword,
    forgotSetNewPassword: DynamicStepForgotSetNewPassword,
    // registerVerifyOTP: DynamicStepRegisterVerifyOTP,
    // setNewPassword: DynamicStepSetNewPassword,
  }[page.step]

  return (
    <Modal
      isOpen={props.isOpen}
      style={customStyles}
      onRequestClose={props.closeModal}
      ariaHideApp={false}
    >
      <div className={styles.header}>
        <button
          className={clsx(styles.back, (!isActive || !pageData?.backPage) && styles.fade)}
          onClick={() => changePage({ page: pageData?.backPage, data: { ...page.data } })}
          disabled={!pageData?.backPage}
        />
        <span className={clsx(!isActive && styles.fade)}>{pageData?.title}</span>
        <button className={styles.close} onClick={props.closeModal} />
      </div>
      <div className={clsx(styles.body, !isActive && styles.fade)}>
        <DynamicComponent changePage={changePage} data={page.data} />
      </div>
    </Modal>
  )
}

RegisterModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
}
