import Icon from '@components/common/icon'
import { apiUpdatePassword } from '@features/api/member'
import { cookieUpdateAuthData, getAuthData } from '@utils/helper/member/auth'
import { useAuth } from '@utils/helper/member/auth'
import { usePasswordValidation } from '@utils/helper/member/usePasswordValidation'
import clsx from 'clsx'
import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'

import styles from '../styles/login/stepRegisterSetPassword.module.scss'

const DynamicTermCondition = dynamic(() => import('./modalTermCondition'), { ssr: false })

export default function StepRegisterSetNewPassword({ changePage, data }) {
  const [isOpenTandC, setIsOpenTandC] = useState(false)
  const [values, setValues] = useState({
    showPassword: false,
  })
  const [termsChecked, setTermsChecked] = useState(false)
  const [action] = useState(data?.action)
  // const [consent, setConsent] = useState(false)

  const [verifiedPassword, setVerifiedPassword] = useState(false)
  const [password, setPassword] = useState({
    firstPassword: '',
    confirmPassword: '',
  })
  const [validLength, hasNumber, upperCase, lowerCase, match] = usePasswordValidation({
    firstPassword: password.firstPassword,
    confirmPassword: password.confirmPassword,
  })
  const { setUser } = useAuth()
  const [errorMessage, setErrorMessage] = useState('')

  const handleClickShowPassword = (type) => {
    setValues({
      ...values,
      showPassword: type === 'password' ? !values.showPassword : values.showPassword,
      showConfirmPassword:
        type === 'confirmPassword' ? !values.showConfirmPassword : values.showConfirmPassword,
    })
  }

  const handleChangePassword = (e) => {
    const curPassword = e.target.value
    setPassword({ ...password, firstPassword: curPassword })
  }

  const handleChangeConfirmPassword = (e) => {
    const curConfirmPassword = e.target.value
    setPassword({ ...password, confirmPassword: curConfirmPassword })
  }

  useEffect(() => {
    if (validLength && hasNumber && upperCase && lowerCase && match && termsChecked) {
      setVerifiedPassword(true)
    } else {
      setVerifiedPassword(false)
    }
  }, [validLength, hasNumber, upperCase, lowerCase, match, termsChecked])

  const handleTerms = (e) => {
    setTermsChecked(!termsChecked)
  }

  const onSubmit = async () => {
    if (verifiedPassword) {
      const token = data?.auth.token
      const response = await apiUpdatePassword(token, {
        username: data?.auth.username,
        password: password.firstPassword,
        type: 'phone',
      })
      if (response.code === 'success' && cookieUpdateAuthData(response.data)) {
        const userData = getAuthData()
        setUser(userData)
        changePage({ page: 'registerSuccess' })
      } else {
        alert('Something went worng.')
      }
    }
  }

  return (
    <>
      {isOpenTandC && <DynamicTermCondition closeModal={() => setIsOpenTandC(false)} />}
      <div className={styles.middle}>
        <p className={styles['text-field']}>
          <input
            type={values.showPassword ? 'text' : 'password'}
            value={password.firstPassword}
            onChange={handleChangePassword}
            required
          />
          <span className={styles['floating-label']}>รหัสผ่าน</span>
          <button onClick={() => handleClickShowPassword('password')}>
            <Icon path={values.showPassword ? 'eye' : 'eye-off'} w="20" h="20" strokeWidth="0.1" />
          </button>
        </p>

        <div className={styles['condition-wrapper']}>
          <p className={clsx(upperCase && lowerCase && styles.passed)}>
            <Icon path="check" w="14" h="14" /> ตัวอักษร A-Z และ a-z
          </p>
          <p className={clsx(hasNumber && styles.passed)}>
            <Icon path="check" w="14" h="14" /> เลข 0-9 อย่างน้อย 1 ตัว
          </p>
          <p className={clsx(validLength && styles.passed)}>
            <Icon path="check" w="14" h="14" /> มีความยาวรวมกันไม่ต่ำกว่า 6 ตัวขึ้นไป
          </p>
        </div>

        <p className={styles['text-field']}>
          <input
            type={values.showConfirmPassword ? 'text' : 'password'}
            value={password.confirmPassword}
            onChange={handleChangeConfirmPassword}
            required
          />
          <span className={styles['floating-label']}>ยืนยันรหัสผ่าน</span>
          <button onClick={() => handleClickShowPassword('confirmPassword')}>
            <Icon
              path={values.showConfirmPassword ? 'eye' : 'eye-off'}
              w="20"
              h="20"
              strokeWidth="0.1"
            />
          </button>
        </p>
        {password.confirmPassword !== '' && !match ? (
          <div className={`${styles['text-help']} ${!match ? styles['error'] : ''}`}>
            ยืนยันรหัสผ่านไม่ตรงกัน
          </div>
        ) : (
          ''
        )}
      </div>

      <div className={styles.bottom}>
        <div className={styles.terms}>
          <input
            type="checkbox"
            id="termsAndCondition"
            onChange={handleTerms}
            defaultChecked={termsChecked}
          />
          <p>
            ข้าพเจ้าได้อ่านและยอมรับ{' '}
            <span className={styles['txt-link']} onClick={() => setIsOpenTandC(true)}>
              ข้อกำหนดและเงื่อนไข และรับทราบหนังสือแจ้งรายละเอียดการเก็บรวบรวมใช้
              และเปิดเผยข้อมูลส่วนบุคคลของสมาชิกบิ๊กซีออนไลน์
            </span>
          </p>
        </div>
        {/* <div className={styles.terms}>
          <input type="checkbox" id="terms2" defaultChecked />
          <p>
            ข้าพเจ้ายินยอม{' '}
            <span className={styles['txt-link']}>
              ให้บริษัทเปิดเผยข้อมูลส่วนบุคคลตามเอกสารนี้ทุกประการ
            </span>
          </p>
        </div> */}
        <button className={styles.next} disabled={!verifiedPassword} onClick={onSubmit}>
          ยืนยัน
        </button>
      </div>
    </>
  )
}

StepRegisterSetNewPassword.propTypes = {
  changePage: PropTypes.func,
  data: PropTypes.object,
}
