import PropTypes from 'prop-types'
import Modal from 'react-modal'

import styles from './styles/addAddress.module.scss'
import defaultStyles from './styles/defaultStyles'

const customStyles = {
  overlay: { ...defaultStyles.overlay },
  content: {
    ...defaultStyles.content,
    width: '600px',
    height: '90%',
    animation: 'upBoxAnimation 0.9s',
  },
}

export default function AddAddressModal(props) {
  const { eventHandler } = props
  return (
    <Modal
      isOpen={props.isOpen}
      style={customStyles}
      onRequestClose={props.closeModal}
      ariaHideApp={false}
    >
      <div className={styles.header}>
        เพิ่มที่อยู่จัดส่งสินค้า
        <button className={styles.close} onClick={props.closeModal}></button>
      </div>
      <div className={styles.body}>
        <div className={styles.row}>
          <div className={styles.col}>
            <label className="form-label">ชื่อ *</label>
            <input type="text" placeholder="กรุณากรอกชื่อ" />
          </div>
          <div className={styles.col}>
            <label className="form-label">นามสกุล *</label>
            <input type="text" placeholder="กรุณากรอกนามสกุล" />
          </div>
        </div>

        <div className={styles.row}>
          <div className={styles.col}>
            <label className="form-label">เบอร์โทรศัพท์ *</label>
            <input type="text" placeholder="กรุณากรอกเบอร์โทรศัพท์" />
          </div>
        </div>

        <div className={styles.row}>
          <div className={styles.col}>
            <div className={styles.row} style={{ marginBottom: 0 }}>
              <div className={styles.col}>
                <label className="form-label">บ้านเลขที่ *</label>
                <input type="text" placeholder="บ้านเลขที่" />
              </div>
              <div className={styles.col}>
                <label className="form-label">หมายเลขห้อง</label>
                <input type="text" placeholder="หมายเลขห้อง" />
              </div>
            </div>
          </div>
          <div className={styles.col}>
            <label className="form-label">หมู่บ้าน / อาคาร</label>
            <input type="text" placeholder="อาคาร / ชั้น / ออฟฟิศ" />
          </div>
        </div>

        <div className={styles.row}>
          <div className={styles.col}>
            <label className="form-label">บริษัท</label>
            <input type="text" placeholder="ระบุชื่อบริษัท" />
          </div>
          <div className={styles.col}>
            <label className="form-label">หมู่</label>
            <input type="text" placeholder="หมู่" />
          </div>
        </div>

        <div className={styles.row}>
          <div className={styles.col}>
            <label className="form-label">ซอย</label>
            <input type="text" placeholder="ซอย" />
          </div>
          <div className={styles.col}>
            <label className="form-label">ถนน *</label>
            <input type="text" placeholder="ถนน" />
          </div>
        </div>

        <div className={styles.row}>
          <div className={styles.col}>
            <label className="form-label">จังหวัด *</label>
            <select>
              <option disabled selected>
                กรุณาเลือกจังหวัด
              </option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div className={styles.col}>
            <label className="form-label">อำเภอ *</label>
            <select>
              <option disabled selected>
                กรุณาเลือกอำเภอ
              </option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
        </div>

        <div className={styles.row}>
          <div className={styles.col}>
            <label className="form-label">ตำบล *</label>
            <select>
              <option disabled selected>
                กรุณาเลือกตำบล
              </option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div className={styles.col}>
            <label className="form-label">รหัสไปรษณีย์ *</label>
            <input type="text" placeholder="รหัสไปรษณีย์" />
          </div>
        </div>

        <div className={styles.selector}>
          <input type="checkbox" id="use_main_address" />
          <label htmlFor="use_main_address">ใช้เป็นที่อยู่หลัก</label>

          <label className={styles.switch}>
            <input type="checkbox" id="use_tax_address" />
            <span />
          </label>
          <label htmlFor="use_tax_address">ใช้เป็นที่อยู่ใบกำกับภาษี</label>
        </div>
      </div>
      <div className={styles.footer}>
        <button onClick={() => eventHandler()}>บันทึกที่อยู่</button>
      </div>
    </Modal>
  )
}

AddAddressModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  eventHandler: PropTypes.func.isRequired,
}
