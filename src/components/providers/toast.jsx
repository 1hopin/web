import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'
import { createContext, useState } from 'react'

const DynamicToastComponent = dynamic(() => import('../common/toast'), { ssr: false })

export const ToastContext = createContext()
export function ToastProvider({ children }) {
  const [isExecute, setIsExecute] = useState(false)
  const [toastData, setToastData] = useState({})

  function toast(data) {
    if (!isExecute) setIsExecute(true)
    if (typeof data === 'string') setToastData({ id: Date.now(), type: 'success', msg: data })
    else setToastData({ id: Date.now(), type: data.type, msg: data.msg })
  }

  return (
    <ToastContext.Provider value={{ toast }}>
      {children}
      {isExecute && <DynamicToastComponent options={toastData} />}
    </ToastContext.Provider>
  )
}

ToastProvider.propTypes = {
  children: PropTypes.node.isRequired,
}
