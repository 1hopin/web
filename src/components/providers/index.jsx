/* eslint-disable react/no-children-prop */

import PropTypes from 'prop-types'
import { I18nextProvider } from 'react-i18next'

import i18n from '../../libs/i18n'
import { AuthProvider } from './auth'
import { ModalProvider } from './modal'
import { ToastProvider } from './toast'
import { UserAgentProvider } from './userAgent'

export default function Providers(props) {
  const { userAgent, authDataFromServer } = props
  return (
    <Compose
      components={[
        [I18nextProvider, { i18n }],
        [UserAgentProvider, { userAgent }],
        [AuthProvider, { authDataFromServer }],
        ModalProvider,
        ToastProvider,
      ]}
      children={props.children}
    />
  )
}

function Compose({ components, children }) {
  return components.reduceRight((prev, curr) => {
    if (Array.isArray(curr)) {
      const [Component, props] = curr
      return <Component {...props}>{prev}</Component>
    }

    const Component = curr
    return <Component>{prev}</Component>
  }, children)
}

Providers.propTypes = {
  userAgent: PropTypes.object.isRequired,
  authDataFromServer: PropTypes.string,
  children: PropTypes.node.isRequired,
}
