import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'
import { createContext, useReducer, useState } from 'react'

const DynamicLoginModal = dynamic(() => import('../modals/loginRegister'), { ssr: false })
const DynamicAddressModal = dynamic(() => import('../modals/addAddress'), { ssr: false })
const DynamicFeedBackModal = dynamic(() => import('../modals/feedback'), { ssr: false })

let DynamicModalComponent
export const ModalContext = createContext()
export function ModalProvider({ children }) {
  const [, forceUpdate] = useReducer((x) => x + 1, 0)
  const [isOpen, setIsOpen] = useState(false)
  const closeModal = () => setIsOpen(false)

  function openModal(type) {
    DynamicModalComponent = {
      login: DynamicLoginModal,
      addAddress: DynamicAddressModal,
      feedback: DynamicFeedBackModal,
    }[type]
    isOpen ? forceUpdate() : setIsOpen(true)
  }

  return (
    <ModalContext.Provider value={{ openModal, closeModal }}>
      {children}
      {isOpen && <DynamicModalComponent isOpen={isOpen} closeModal={closeModal} />}
    </ModalContext.Provider>
  )
}

ModalProvider.propTypes = {
  children: PropTypes.node.isRequired,
}
