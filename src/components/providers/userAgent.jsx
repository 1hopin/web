import PropTypes from 'prop-types'
import { createContext } from 'react'

export const UserAgentContext = createContext()
export function UserAgentProvider({ userAgent, children }) {
  return <UserAgentContext.Provider value={userAgent}>{children}</UserAgentContext.Provider>
}

UserAgentProvider.propTypes = {
  userAgent: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
}
