import { b64Decode } from '@utils/helper/common/base64'
import { removeCookie } from '@utils/helper/common/cookie'
import PropTypes from 'prop-types'
import { createContext, useState } from 'react'

export const AuthContext = createContext()

function getAuthInitialState(authDataFromServer) {
  return () => {
    if (authDataFromServer) {
      return JSON.parse(b64Decode(authDataFromServer))
    }
    return null
  }
}

export function AuthProvider({ authDataFromServer, children }) {
  const [user, setUser] = useState(getAuthInitialState(authDataFromServer))
  const cookieName = 'profile'

  function logout() {
    setUser(null)
    removeCookie(cookieName)
  }

  return (
    <AuthContext.Provider
      value={{
        isAuthenticated: !!user,
        user,
        logout,
        setUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

AuthProvider.propTypes = {
  authDataFromServer: PropTypes.string,
  children: PropTypes.node.isRequired,
}
