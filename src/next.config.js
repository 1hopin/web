/** @type {import('next').NextConfig} */
require('dotenv').config()
const Dotenv = require('dotenv-webpack')
const path = require('path')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

const nextConfig = {
  eslint: {
    ignoreDuringBuilds: true,
  },
  swcMinify: true,
  reactStrictMode: true,
  assetPrefix: process.env.ASSET_PREFIX,
  images: {
    domains: [
      'spoadmin-stg.bigc.co.th', // image in checkout
      'nonprod-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com',
      'nonprod-dev-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com', // image in cart
      'nonprod-uat-s3-bigc-a1.s3.ap-southeast-1.amazonaws.com',
    ],
  },
  webpack: (config) => {
    config.plugins = [
      ...config.plugins,
      new Dotenv({
        path: path.join(process.cwd(), '.env'),
        systemvars: true,
      }),
    ]

    if (config.optimization.splitChunks.cacheGroups) {
      config.optimization.splitChunks.cacheGroups['libs'] = {
        test: /[\\/]node_modules[\\/](@babel|next|react-i18next|clsx)[\\/]/,
        name: 'libs',
      }
      config.optimization.splitChunks.cacheGroups['modules'] = {
        test: /[\\/]components[\\/]common[\\/](icon)|[\\/]utils[\\/](icon)/,
        name: 'modules',
        enforce: true,
      }
    }

    return config
  },
}

module.exports = withBundleAnalyzer(nextConfig)
