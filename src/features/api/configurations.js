import { fetchGet } from '@utils/api/_helper'

export function getHomeConfig() {
  return fetchGet({ path: '/cms/v1/shared/configurations?page=home' })
}

export function getFeaturesConfig() {
  return fetchGet({ path: '/api/configurations/features' })
}
