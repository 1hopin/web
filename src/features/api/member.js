import { fetchPost } from '@utils/api/_helper'

export async function register(data) {
  return fetchPost({
    host: process.env.HOST,
    path: `/api/member/register`,
    options: {
      data: {
        firstname: data?.firstname,
        lastname: data?.lastname,
        phone_no: data?.phone_no
      },
    },
  })
}

export async function login(data) {
  return fetchPost({
    host: process.env.HOST,
    path: `/api/member/login`,
    options: {
      data: {
        token: data?.token,
        pin: data?.pin,
        phone_no: data?.phone_no
      },
    },
  })
}

export async function profile(data) {
  return fetchPost({
    host: process.env.HOST,
    path: `/api/member/profile`,
    options: {
      data: {
        token: data?.token,
      },
    },
  })
}