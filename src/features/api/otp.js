import { fetchPost } from '@utils/api/_helper'
export async function requestOTP(phoneNumber) {
  return fetchPost({
    host: process.env.HOST,
    path: `/api/otp/request`,
    options: {
      data: {
        phone_no: phoneNumber,
      },
    },
  })
}

export async function verifyOTP(token,code) {
  return fetchPost({
    host: process.env.HOST,
    path: `/api/otp/verify`,
    options: {
      data: {
        token: token,
        pin: code,
      },
    },
  })
}