export function getAsPathByPathName(pathname, params = {}) {
  let unusedParams = ''

  const asPath = Object.keys(params).reduce((prev, cur) => {
    const inUsed = pathname.indexOf(`[${cur}]`)
    if (inUsed === -1) {
      unusedParams = unusedParams + `&${cur}=${params[cur]}`
    }

    return `${prev.replace(`[${cur}]`, params[cur])}`
  }, pathname)

  const queryString = unusedParams !== '' ? '?' + trimStart(unusedParams, '&') : ''

  return `${asPath}${queryString}`
}
