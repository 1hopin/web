import { useEffect, useState } from 'react'

export default function otpCountDown() {
  const amountTotal = 5 * 60 // 5 minutes for timer
  let timeout = 0
  const [seconds, setSeconds] = useState(amountTotal)

  const countSeconds = seconds % 60
  const countMinutes = Math.floor(seconds / 60)

  useEffect(() => {
    if (seconds > 0) {
      timeout = setTimeout(() => {
        setSeconds((state) => state - 1)
      }, 1000)
    } else {
      clearTimeout(timeout)
    }
  }, [seconds])

  return [countMinutes, countSeconds]
}
