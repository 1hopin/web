import { AuthContext } from '@components/providers/auth'
import { useContext } from 'react'

import { b64Decode, b64Encode } from '../common/base64'
import { getCookie, setCookie } from '../common/cookie'

const cookieName = 'profile'

export function useAuth() {
  return useContext(AuthContext)
}

/**
 *
 * @param {*} data
 * @returns Boolean
 */
export function authEncrypt(data) {
  setCookie(cookieName, b64Encode(data), 7)
  return !!getCookie(cookieName)
}

/**
 *
 * @returns Object
 */
export function getAuthData() {
  if (!getCookie(cookieName)) return false

  const profileBase64 = getCookie(cookieName)
  return JSON.parse(b64Decode(profileBase64))
}

export function displayFullname(user, length = 15) {
  if (user.customer?.first_name) {
    const fullname = user?.customer.first_name + ' ' + user?.customer.first_name
    return `${fullname.slice(0, length)}..`
  } else {
    return user.customer.phone_no
  }
}

export function numberZeroPrefix(number, len = 2) {
  return `${number}`.padStart(len, '0')
}

export function cookieUpdateAuthData(authData) {
  try {
    const cookieUserData = getAuthData()
    const data = { ...cookieUserData, ...{ auth: authData } }
    authEncrypt(JSON.stringify(data))
    return true
  } catch (e) {
    return false
  }
}

export function cookieUpdateUserData(userData) {
  try {
    const cookieUserData = getAuthData()
    const data = { ...cookieUserData, ...{ customer: userData } }
    authEncrypt(JSON.stringify(data))
    return true
  } catch (e) {
    return false
  }
}
