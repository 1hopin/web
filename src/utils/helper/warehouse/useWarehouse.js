import { getAllWarehouseData, getWarehouseByLatLong } from '@features/api/checkout'
import { useEffect, useState } from 'react'

function useWarehouse(location = false) {
  const [warehouseData, setWarehouseData] = useState(null)
  async function getPosition(position) {
    const latitude = position?.coords?.latitude
    const longitude = position?.coords?.longitude
    const warehouseData = await getWarehouseByLatLong({
      lat: latitude,
      long: longitude,
      count: 5,
    })

    if (warehouseData?.code === 'success') {
      //   checkoutDispatch({ type: 'pickup', payload: warehouseData?.data?.warehouse_stores[0] })
      setWarehouseData(warehouseData?.data)
      //   setUserPosition(true)
      //   setIsLoading(false)
    } else {
      //error case
    }
    // console.log({ warehouseData })
  }

  useEffect(() => {
    const fetchData = async () => {
      const warehouseData = await getAllWarehouseData()
      if (warehouseData?.code === 'success') {
        setWarehouseData(warehouseData?.data)
        // setIsLoading(false)
      } else {
        alert('get all warehouse list from api')
        // error
      }
    }

    if ('geolocation' in navigator && location) {
      // console.log('geolocation')
      navigator.geolocation.getCurrentPosition(getPosition, function (error) {
        // can't get user geo
        fetchData()
        console.log(error.message)
      })
    } else {
      // console.log('no-geolocation')
      fetchData()
    }
  }, [location])

  return warehouseData
}

export { useWarehouse }
