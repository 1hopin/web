export default function generateProductSlugUrl(product) {
  let productName = (product.slug !== '' ? product.slug : product.name_en) || ''

  let productSlug = productName
  if (product.slug === '') {
    productSlug = productSlug.toLowerCase()
    productSlug = productSlug.replaceAll(' ', '-')
    // console.log('Hello > ', productSlug, ' > ', product.name_en)
  }

  return `${productSlug}.${product.product_id}`
}
