export default function calculateProductPrice({ data, lang }) {
  // product's base price
  let basePrice = data?.price_incl_tax || ''
  // basePrice = basePrice.toFixed(2)

  // product's final price
  const finalPrice = data?.final_price_incl_tax || ''

  // product's flashdeal price
  const flashdealPrice = data?.flashdeal_price || ''

  const productUnit = lang === 'en' ? data?.unit_en || 'Each' : data?.unit || 'ชิ้น'

  let salePrice = ''
  if (flashdealPrice) {
    salePrice = flashdealPrice.toFixed(2) || 0
  } else {
    salePrice = (finalPrice && finalPrice.toFixed(2)) || 0
  }

  const productSalePrice = {
    type: salePrice < basePrice || flashdealPrice ? 'promotion' : 'normal',
    basePrice,
    flashdealPrice,
    productUnit,
    salePrice,
  }

  return productSalePrice
}
