export default function getProductQueryId(slug) {
  const lastIndexOfSeparator = slug.lastIndexOf('.')
  const productStringId = slug.slice(lastIndexOfSeparator, slug.lengtht)
  const queryId = productStringId.substring(productStringId.indexOf('.') + 1)
  return queryId
}
