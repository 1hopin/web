import { UserAgentContext } from '@components/providers/userAgent'
import { useContext } from 'react'
import parser from 'ua-parser-js'

export function useDevice() {
  const { device } = useContext(UserAgentContext)
  return device
}

export function getUserAgent(req) {
  const ua = parser(req.headers['user-agent'])
  return {
    device: {
      isTablet: ua.device.type === 'tablet',
      isMobile: ua.device.type === 'mobile',
      isElectron: ua.browser.name === 'Electron',
    },
  }
}
