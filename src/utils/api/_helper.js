// fetch request
const defaultTimeout = 10000
import axios from 'axios'

export async function fetcher({
  host = process.env.API_URL,
  path,
  params,
  options,
  timeout = defaultTimeout,
}) {
  const protocol = 'http:'
  const apiOptions = {
    url: `${protocol}${host}${path}`,
    method: options.method,
    params,
    headers: {
      'Content-Type': 'application/json',
      ...(options?.token && { Token: options.token }),
      ...options?.headers,
    },
    timeout,
  }

  if (options?.data) {
    apiOptions['data'] = options.data
  }

  console.log('apiOptions',apiOptions);

  return axios(apiOptions)
    .then(({ data }) => {
      return data
    })
    .catch((err) => {
      if (err.response) {
        return err.response.data
      } else {
        console.error('[Fetcher - error] > ', err)
      }
    })
}

export async function fetchPost({ host,path, params, options }) {
  return fetcher({ host,path, params, options: { ...options, method: 'post' } })
}

export async function fetchGet({ host,path, params, options }) {
  return fetcher({ host,path, params, options: { ...options, method: 'get' } })
}

export async function fetchPatch({ host,path, params, options }) {
  return fetcher({ host,path, params, options: { ...options, method: 'patch' } })
}
