const next = require('next')
const i18n = require('./libs/i18n')
const express = require('express')
const i18nextMiddleware = require('i18next-http-middleware')
const Backend = require('i18next-fs-backend')
const path = require('path')

const port = 9000
const app = next({ dev: process.env.NODE_ENV !== 'production', port, dir: 'src' })
const handle = app.getRequestHandler()


i18n.use(Backend).init(
  {
    ns: ['common', 'auth'],
    lng: 'th',
    load: 'languageOnly',
    defaultNS: 'common',
    backend: { loadPath: path.resolve('./src/public') + '/locales/{{lng}}/{{ns}}.json' },
  },
  () => {
    app.prepare().then(() => {
      const server = express()
      server.use(i18nextMiddleware.handle(i18n))

      server.use((req, res) => {
        handle(req, res)
      })

      server.listen(port, (err) => {
        if (err) throw err
        console.log(`> Ready on http://localhost:${port}`)
      })
    })
  },
)
