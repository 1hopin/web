import { setCookie } from '@utils/helper/common/cookie'

import i18n from '.'

export function getI18nData(context, pageNamespaces) {
  if (i18n.language !== context.req.language) i18n.changeLanguage(context.req.language)
  return { i18nResourceData: getResourceStore(pageNamespaces) }
}

function getResourceStore(pageNamespaces) {
  const i18nResourceStore = {}
  if (typeof pageNamespaces === 'string') {
    pageNamespaces = [pageNamespaces]
  }

  let namespaces = [i18n.options.defaultNS] // Add default namespace
  if (Array.isArray(pageNamespaces)) {
    namespaces = [...namespaces, ...pageNamespaces]
  }
  for (const ns of namespaces) {
    i18nResourceStore[ns] = i18n.getResourceBundle(i18n.language, ns)
  }

  return i18nResourceStore
}

export function handleI18n({ language, i18nResourceData }) {
  for (const ns in i18nResourceData) {
    if (!i18n.hasResourceBundle(language, ns)) {
      i18n.addResourceBundle(language, ns, i18nResourceData[ns])
    }
  }

  if (language !== i18n.language) i18n.changeLanguage(language)
}

export async function changeLanguage(newLang) {
  for (const ns of i18n.options.ns) {
    if (!i18n.hasResourceBundle(newLang, ns)) {
      await loadNamespace(ns, newLang)
    }
  }

  await i18n.changeLanguage(newLang)
  keepLangToCK(newLang)
}

export async function loadNamespace(ns, locale) {
  const fileURL = `${process.env.HOST}/locales/${locale}/${ns}.json`
  // const fileURL = `http://10.10.40.52:8087/master/v1/localization/web/${ns}/${locale}`
  const jsonResult = await fetch(fileURL)

  i18n.addResourceBundle(locale, ns, await jsonResult.json())
}

export function keepLangToCK(language) {
  setCookie('language', language, 365)
}
