const i18n = require('i18next')

if (typeof window !== 'undefined') {
  const options = {
    ns: [],
    lng: 'th',
    load: 'languageOnly',
    initImmediate: false,
  }
  i18n.init(options)
}

module.exports = i18n
