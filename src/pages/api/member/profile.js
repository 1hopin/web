import { fetchGet } from '@utils/api/_helper'
export default async function profile(req, res) {
  console.log('get profile token', req.body.token)
  fetchGet({
    path: `/customer/v1/customers/profile`,
    options: {
      token:req.body.token,
    },
  }).then(result => {
    const mockResult = {
      "code": "success",
      "data": {
          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NTU5MDYwNzUsImlkIjo2M30.P73mP7S9-LSI4sQT17HJXOHmQh1YdiBKOVv6FmqB6HM"
      },
      "message": "success"
  }
    return res.status(200).json(result)
  })
  .catch(e => {
    return res.status(401).json(e)
  })
}