import { fetchPost } from '@utils/api/_helper'
export default async function register(req, res) {
  console.log('register member firstname', req.body.firstname)
  console.log('register member lastname', req.body.lastname)
  console.log('register member phone_no', req.body.phone_no)
  fetchPost({
    path: `/customer/v1/customers`,
    options: {
      data: {
        firstname:req.body.firstname,
        lastname:req.body.lastname,
        phone_no:req.body.phone_no,
      },
    },
  }).then(result => {
    const mockResult = {
      "code": "success",
      "data": {
          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NTU5MDYwNzUsImlkIjo2M30.P73mP7S9-LSI4sQT17HJXOHmQh1YdiBKOVv6FmqB6HM"
      },
      "message": "success"
  }
    return res.status(200).json(result)
  })
  .catch(e => {
    return res.status(401).json(e)
  })
}