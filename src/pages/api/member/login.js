import { fetchPost } from '@utils/api/_helper'
export default async function login(req, res) {
  console.log('login member token', req.body.token)
  console.log('login member pin', req.body.pin)
  console.log('login member phone_no', req.body.phone_no)
  fetchPost({
    path: `/customer/v1/signin`,
    options: {
      data: {
        token:req.body.token,
        pin:req.body.pin,
        phone_no:req.body.phone_no,
      },
    },
  }).then(result => {
    return res.status(200).json(result)
  })
  .catch(e => {
    return res.status(401).json(e)
  })
}