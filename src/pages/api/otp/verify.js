import { fetchPost } from '@utils/api/_helper'
export default async function verify(req, res) {
  console.log('verify token', req.body.token)
  console.log('verify pin', req.body.pin)
  fetchPost({
    path: `/sms/v1/otps/verify`,
    options: {
      data: {
        token:req.body.token,
        pin:req.body.pin,
      },
    },
  }).then(result => {
    return res.status(200).json(result)
  })
  .catch(e => {
    return res.status(401).json(e)
  })
}