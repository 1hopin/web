import { fetchPost } from '@utils/api/_helper'
export default async function request(req, res) {
  console.log('requestOTP phone_no', req.body.phone_no)
  fetchPost({
    path: `/customer/v1/otps/request`,
    options: {
      data: {
        phone_no:req.body.phone_no,
      },
    },
  }).then(result => {
    const mockResult = {
      "code": "success",
      "data": {
          "otp": {
              "refno": "19OIB",
              "status": "success",
              "token": "ynwQMZxd73mvV0Aslf8Jrj9Bbq2G56LA"
          },
          "type": {
              "is_type": "new"
          }
      },
      "message": "success"
  }
    return res.status(200).json(result)
  })
  .catch(e => {
    return res.status(401).json(e)
  })
}