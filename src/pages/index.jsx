import withDynamicRendering from '@components/hoc/withDynamicRendering'
import metaHome from '@configs/meta/home'
import { useDevice } from '@utils/userAgent'
import dynamic from 'next/dynamic'

const HomeDesktop = dynamic(() => import('../components/home/desktop'))
const HomeMobile = dynamic(() => import('../components/home/mobile'))

export default function HomePage(props) {
  const { isMobile } = useDevice()
  return isMobile ? <HomeMobile {...props} /> : <HomeDesktop {...props} />
}

export async function getServerSideProps(context) {
  const enhancedFetchData = await withDynamicRendering()(fetchData)
  return enhancedFetchData(context)
}

async function fetchData(context) {
  const seoData = {
    dynamicMeta: {
      ...metaHome,
    },
  }

  return {
    props: {
      seoData,
    },
  }
}
