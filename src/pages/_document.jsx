import Document, { Head, Html, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    initialProps.isMobile = ctx.req?.ua?.device?.isMobile

    return initialProps
  }

  render() {
    return (
      <Html className={this.props.isMobile ? 'mobile' : 'desktop'}>
        <Head />
        <body className='rounded ltr background'>
            <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
export default MyDocument
