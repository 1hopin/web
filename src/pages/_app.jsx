import '@styles/index.scss'
import '@assets/css/vendor/bootstrap.min.css';
import '@assets/css/vendor/bootstrap.rtl.only.min.css';
import 'react-circular-progressbar/dist/styles.css';
import 'react-perfect-scrollbar/dist/css/styles.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'react-image-lightbox/style.css';

import '@assets/css/sass/themes/gogo.light.1hopin.scss';

import withApp from '@components/hoc/withApp'
import Layout from '@components/layout'
import Providers from '@components/providers'
import { handleI18n } from '@libs/i18n/_helper'
import Head from 'next/head'
import PropTypes from 'prop-types'

function MyApp({ Component, pageProps }) {
  typeof window !== 'undefined' && handleI18n(pageProps)

  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Providers {...pageProps}>
        <Layout {...pageProps}>
          <Component {...pageProps} />
        </Layout>
      </Providers>
    </>
  )
}

MyApp.propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.object.isRequired,
}

export default withApp(MyApp)
