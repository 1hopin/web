import { useDevice } from '@utils/userAgent'
import dynamic from 'next/dynamic'
const ErrorDesktop = dynamic(() => import('../components/error/desktop'))
const ErrorMobile = dynamic(() => import('../components/error/mobile'))

export default function ErrorPage(props) {
  const { isMobile } = useDevice()
  return isMobile ? <ErrorMobile {...props} /> : <ErrorDesktop {...props} />
}
