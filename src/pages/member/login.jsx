import { useState } from 'react';
import { setCookie } from '@utils/helper/common/cookie'
import { useRouter } from 'next/router'
import withDynamicRendering from '@components/hoc/withDynamicRendering'
import metaLogin from '@configs/meta/login'
import { useDevice } from '@utils/userAgent'
import { useTranslation } from 'react-i18next'
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';
import Link from 'next/link'

import { requestOTP,verifyOTP } from '@features/api/otp'
import { login, profile, register } from '@features/api/member'

import { Formik, Form, Field } from 'formik';

import { Colxx } from 'components/common/CustomBootstrap';


// LoginState Declarations
// [0] Common Case -> Request MobileNumber Input
// [1] Common Case -> Request OTP Input
// [2] New Member Case -> Warning click to registration page (3)
// [3] New Member Case -> Request Fristname Lastname Email
// [4] Old Member Case -> Redirect to MemberPage


export default function LoginPage() {
  const router = useRouter()
  const { t } = useTranslation('common')
  const [loginState,setLoginState] = useState(0);
  const [mobileNumber,setMobileNumber] = useState('');
  const [otpToken,setOtpToken] = useState('');
  const [refNo,setRefNo] = useState('');
  const [type,setType] = useState('');
  const [firstName,setFirstName] = useState('');
  const [lastName,setLastName] = useState('');
  const [otpNumber,setOtpNumber] = useState('');
  const [errors,setErrors] = useState({
    mobileNumber:false,
    firstName:false,
    lastName:false,
    otpNumber:false,
    login:false,
  });
  const initialValues = { mobileNumber };

  const onSubmitMobileNumber = async () => {
    if (mobileNumber.length == 10) {
      const result = await requestOTP(mobileNumber)
      console.log('onSubmitMobileNumber result',result)
      if(result?.code === 'success'){
        console.log('onSubmitMobileNumber success')
        await setLoginState(1)
        await setOtpToken(result.data?.otp.token)
        await setRefNo(result.data?.otp.refno)
        await setType(result.data?.type.is_type)
      }
    }
  }

  const onRetryOTP = async () => {
    if (mobileNumber.length == 10) {
      const result = await requestOTP(mobileNumber)
      console.log('onRetryOTP result',result)
      if(result?.code === 'success'){
        console.log('onRetryOTP success')
        await setLoginState(1)
        await setOtpToken(result.data?.otp.token)
        await setRefNo(result.data?.otp.refno)
        await setType(result.data?.type.is_type)
      }
    }
  }

  const onChangeMobileNumber = (e) => {
    if(Number.isInteger(e.target.value*1)){
      setMobileNumber(e.target.value)
    }
  };

  const onChangeOTPNumber = (e) => {
    if(Number.isInteger(e.target.value*1)){
      setOtpNumber(e.target.value)
    }
  };

  const onSubmitOTP = async () => {
    if (otpNumber.length == 6) {
      const result = await verifyOTP(otpToken,otpNumber)
      console.log('onSubmitOTP result',result)
      if(result?.code === 'success' && otpNumber !== '888888'){

        if(type==="new"){
          // router.push('/member/register')
          setLoginState(3)
        }else{
          const loginResult = await login({
            token:otpToken,
            pin:otpNumber,
            phone_no:mobileNumber,
          })
          console.log('onSubmitOTP loginResult',loginResult)
          if(loginResult?.code === 'success'){
            const authToken = loginResult?.data.token
            setCookie('authToken', authToken, 1000)
            const userDataresult = await profile({
              token:authToken
            })
            if(userDataresult?.code === 'success'){
              console.log("get userData",userDataresult.data)
              setCookie('userData', JSON.stringify(userDataresult.data), 1000)
            }

            
            setTimeout(()=>{
              router.push('/member')
            }, 1000);
            await setLoginState(4)
          }else{
            setErrors({
              ...errors,
              login:true
            })
          }

        }
        
      }else{
        setErrors({
          ...errors,
          otpNumber:true
        })
      }
    }
  }

  const onSubmitUserData = async () => {
    if (firstName && lastName) {
      const result = await register({
        firstname:firstName,
        lastname:lastName,
        phone_no:mobileNumber,
      })
      console.log('onSubmitUserData result',result)
      if(result?.code === 'success'){
        const authToken = result?.data.token
        setCookie('authToken', JSON.stringify(authToken), 1000)
        const userDataresult = await profile({
          token:authToken
        })
        if(userDataresult?.code === 'success'){
          console.log("get userData",userDataresult.data)
          setCookie('userData', JSON.stringify(userDataresult.data), 1000)
        }

        setTimeout(()=>{
          router.push('/member')
        }, 1000);
        await setLoginState(4)

      } else if(result?.code === 'error') {
        if(result?.message === 'is_existed'){
          setRegisterState(1)
        }else{
          //error
        }
      }else{
        //error
      }
    }
  }
  const onChangeFirstName = (e) => {
      setFirstName(e.target.value)
  };
  const onChangeLastName = (e) => {
    setLastName(e.target.value)
  };

  if (loginState === 0) {
    return (
      <div className='container'>
        <Row className="h-100">
          <Colxx xxs="12" md="10" className="mx-auto my-auto">
            <Card className="auth-card">
              <div className="position-relative image-side ">
              </div>
              <div className="form-side">
                <span className="logo-single" />
                <CardTitle className="mb-4">
                เข้าสู่ระบบ
                </CardTitle>
                <Formik initialValues={initialValues} 
                  // onSubmit={onSubmitMobileNumber}
                >
                  {({ errors, touched }) => (
                    <Form className="av-tooltip tooltip-label-bottom">
                      <FormGroup className="form-group has-float-label">
                        <Label>
                          หมายเลขโทรศัพท์มือถือ
                        </Label>
                        <Field
                          className="form-control"
                          id="mobileNumber"
                          name="mobileNumber"
                          type="tel"
                          maxLength="10"
                          autoComplete="off" 
                          value={mobileNumber}
                          onChange={onChangeMobileNumber}
                        />
                        {errors.mobileNumber && touched.mobileNumber && (
                          <div className="invalid-feedback d-block">
                            {errors.mobileNumber}
                          </div>
                        )}
                      </FormGroup>
  
                      <div className="d-flex justify-content-between align-items-center">
                        {/* <span>ยังไม่มีบัญชี? <Link href="/member/register">ลงทะเบียน</Link></span> */}
                        <span>&nbsp;</span>
                        <Button
                          color="primary"
                          type="button"
                          onClick={onSubmitMobileNumber}
                          className={`btn-shadow btn-multiple-state`}
                          size="lg"
                        >
                          <span className="spinner d-inline-block">
                            <span className="bounce1" />
                            <span className="bounce2" />
                            <span className="bounce3" />
                          </span>
                          <span className="label">
                            เข้าสู่ระบบ
                          </span>
                        </Button>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>
            </Card>
          </Colxx>
        </Row>
      </div>
    )
  }else if(loginState === 1){
    return (
      <div className='container'>
        <Row className="h-100">
          <Colxx xxs="12" md="10" className="mx-auto my-auto">
            <Card className="auth-card">
              <div className="position-relative image-side ">
              </div>
              <div className="form-side">
                <span className="logo-single" />
                <CardTitle className="mb-4 card-title-sm">
                ระบบได้ส่งรหัส OTP ไปให้คุณแล้ว (อ้างอิง: {refNo}): <br/>
                กรุณากรอกรหัสที่ได้รับ เพื่อเข้าสู่ระบบ:
                </CardTitle>
                <Formik>
                  {({ }) => (
                    <Form className="av-tooltip tooltip-label-bottom">
                      <FormGroup className="form-group has-float-label">
                        <Label>
                          รหัส OTP ที่ได้รับ
                        </Label>
                        <Field
                          className="form-control"
                          id="otpNumber"
                          name="otpNumber"
                          type="password"
                          maxLength="6"
                          autoComplete="off" 
                          value={otpNumber}
                          onChange={onChangeOTPNumber}
                        />
                        {errors.otpNumber && (
                          <div className="invalid-feedback d-block">
                            OTP ไม่ถูกต้อง
                          </div>
                        )}
                        {errors.login && (
                          <div className="invalid-feedback d-block">
                            ไม่สามารถเข้าสู่ระบบได้
                          </div>
                        )}
                      </FormGroup>
  
                      <div className="d-flex justify-content-between align-items-center">
                        <span onClick={onRetryOTP}>ส่งรหัส OTP อีกครั้ง</span>
                        <Button
                          color="primary"
                          type="button"
                          onClick={onSubmitOTP}
                          className={`btn-shadow btn-multiple-state`}
                          size="lg"
                        >
                          <span className="spinner d-inline-block">
                            <span className="bounce1" />
                            <span className="bounce2" />
                            <span className="bounce3" />
                          </span>
                          <span className="label">
                            เข้าสู่ระบบ
                          </span>
                        </Button>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>
            </Card>
          </Colxx>
        </Row>
      </div>
    )
  }else if(loginState === 2){
    return (
      <div className='container'>
        <Row className="h-100">
          <Colxx xxs="12" md="10" className="mx-auto my-auto">
            <Card className="auth-card">
              <div className="position-relative image-side ">
              </div>
              <div className="form-side">
                <span className="logo-single" />
                <CardTitle className="mb-4 card-title-sm">
                คำเตือน: ระบบไม่พบหมายเลขโทรศัพท์ {mobileNumber} <br/>
                คุณสามารถ ลงทะเบียนฟรี ได้ง่ายๆ
                </CardTitle>
                <Formik>
                  {({ errors, touched }) => (
                    <Form className="av-tooltip tooltip-label-bottom">
                      <div className="d-flex justify-content-between align-items-center">
                        &nbsp;
                        <Button
                          color="primary"
                          type="button"
                          onClick={()=>{
                            setLoginState(3)
                          }}
                          className={`btn-shadow btn-multiple-state`}
                          size="lg"
                        >
                          <span className="spinner d-inline-block">
                            <span className="bounce1" />
                            <span className="bounce2" />
                            <span className="bounce3" />
                          </span>
                          <span className="label">
                            ลงทะเบียน
                          </span>
                        </Button>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>
            </Card>
          </Colxx>
        </Row>
      </div>
    )
  }else if(loginState === 3){
    return (
      <div className='container'>
        <Row className="h-100">
          <Colxx xxs="12" md="10" className="mx-auto my-auto">
            <Card className="auth-card">
              <div className="position-relative image-side ">
              </div>
              <div className="form-side">
                <span className="logo-single" />
                <CardTitle className="mb-4">
                ลงทะเบียน
                </CardTitle>
                <Formik>
                  {({ errors, touched }) => (
                    <Form className="av-tooltip tooltip-label-bottom">
                       <FormGroup className="form-group has-float-label">
                          <Label>
                          ชื่อผู้ใช้ (ภาษาอังกฤษเท่านั้น)
                          </Label>
                          <Field
                            className="form-control"
                            id="firstName"
                            name="firstName"
                            type="text"
                            autoComplete="off" 
                            value={firstName}
                            onChange={onChangeFirstName}
                          />
                          {errors.firstName && touched.firstName && (
                            <div className="invalid-feedback d-block">
                              {errors.firstName}
                            </div>
                          )}
                        </FormGroup>

                        <FormGroup className="form-group has-float-label">
                          <Label>
                          นามสกุล
                          </Label>
                          <Field
                            className="form-control"
                            id="lastName"
                            name="lastName"
                            type="text"
                            autoComplete="off" 
                            value={lastName}
                            onChange={onChangeLastName}
                          />
                          {errors.lastName && touched.lastName && (
                            <div className="invalid-feedback d-block">
                              {errors.lastName}
                            </div>
                          )}
                      </FormGroup>
  
                      <div className="d-flex justify-content-between align-items-center">
                        &nbsp;
                        <Button
                          color="primary"
                          type="button"
                          onClick={onSubmitUserData}
                          className={`btn-shadow btn-multiple-state`}
                          size="lg"
                        >
                          <span className="spinner d-inline-block">
                            <span className="bounce1" />
                            <span className="bounce2" />
                            <span className="bounce3" />
                          </span>
                          <span className="label">
                          ลงทะเบียน
                          </span>
                        </Button>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>
            </Card>
          </Colxx>
        </Row>
      </div>
    )
  }else if(loginState === 4){
    return (
      <div className='container'>
        <Row className="h-100">
          <Colxx xxs="12" md="10" className="mx-auto my-auto">
            <Card className="auth-card">
              <div className="position-relative image-side ">
              </div>
              <div className="form-side">
                <span className="logo-single" />
                <CardTitle className="mb-4 card-title-sm">
                ยืนยันตัวตนสำเร็จ กำลังเข้าสู่ระบบ…
                </CardTitle>
              </div>
            </Card>
          </Colxx>
        </Row>
      </div>
    )
  }
  
}

export async function getServerSideProps(context) {
  const enhancedFetchData = await withDynamicRendering()(fetchData)
  return enhancedFetchData(context)
}

async function fetchData(context) {
  const seoData = {
    dynamicMeta: {
      ...metaLogin,
    },
  }

  return {
    props: {
      seoData,
    },
  }
}
