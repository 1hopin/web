import { useState } from 'react';
import { useRouter } from 'next/router'
import withDynamicRendering from '@components/hoc/withDynamicRendering'
import metaRegister from '@configs/meta/register'
import { useDevice } from '@utils/userAgent'
import { useTranslation } from 'react-i18next'
import { Row, Card, CardTitle, Label, FormGroup, Button } from 'reactstrap';

import Link from 'next/link'
import { register } from '@features/api/member'
import { Formik, Form, Field } from 'formik';

import { Colxx } from 'components/common/CustomBootstrap';


// RegisterState Declarations
// [0] New Uer -> Request Firstname Lastname Number
// [1] Duplicate number -> Warning message 


export default function LoginPage() {
  const router = useRouter()
  const { t } = useTranslation('common')
  const [registerState,setRegisterState] = useState(0);
  const [mobileNumber,setMobileNumber] = useState('');
  const [firstName,setFirstName] = useState('');
  const [lastName,setLastName] = useState('');


  const onChangeMobileNumber = (e) => {
    if(Number.isInteger(e.target.value*1)){
      setMobileNumber(e.target.value)
    }
  };

  const onSubmitUserData = async () => {
    if (firstName && lastName) {
      const result = await register({
        firstname:firstName,
        lastname:lastName,
        phone_no:mobileNumber,
      })
      console.log('onSubmitUserData result',result)
      if(result?.code === 'success'){
        // const authToken = result?.data.token
        // setCookie('authToken', JSON.stringify(authToken), 1000)
        // const userDataresult = await profile({
        //   token:authToken
        // })
        // if(userDataresult?.code === 'success'){
        //   console.log("get userData",userDataresult.data)
        //   setCookie('userData', JSON.stringify(userDataresult.data), 1000)
        // }
        router.push('/member/login')
      } else if(result?.code === 'error') {
        if(result?.message === 'is_existed'){
          setRegisterState(1)
        }else{
          //error
        }
      }else{
        //error
      }
    }
  }

  const onChangeFirstName = (e) => {
      setFirstName(e.target.value)
  };
  const onChangeLastName = (e) => {
    setLastName(e.target.value)
  };

if(registerState===0){
  return (
    <div className='container'>
      <Row className="h-100">
        <Colxx xxs="12" md="10" className="mx-auto my-auto">
          <Card className="auth-card">
            <div className="position-relative image-side ">
            </div>
            <div className="form-side">
              <span className="logo-single" />
              <CardTitle className="mb-4">
              ลงทะเบียน
              </CardTitle>
              <Formik>
                {({ errors, touched }) => (
                  <Form className="av-tooltip tooltip-label-bottom">

                    <FormGroup className="form-group has-float-label">
                      <Label>
                       ชื่อผู้ใช้ (ภาษาอังกฤษเท่านั้น)
                      </Label>
                      <Field
                        className="form-control"
                        id="firstName"
                        name="firstName"
                        type="text"
                        autoComplete="off" 
                        value={firstName}
                        onChange={onChangeFirstName}
                      />
                      {errors.firstName && touched.firstName && (
                        <div className="invalid-feedback d-block">
                          {errors.firstName}
                        </div>
                      )}
                    </FormGroup>

                    <FormGroup className="form-group has-float-label">
                      <Label>
                       นามสกุล
                      </Label>
                      <Field
                        className="form-control"
                        id="lastName"
                        name="lastName"
                        type="text"
                        autoComplete="off" 
                        value={lastName}
                        onChange={onChangeLastName}
                      />
                      {errors.lastName && touched.lastName && (
                        <div className="invalid-feedback d-block">
                          {errors.lastName}
                        </div>
                      )}
                    </FormGroup>

                    <FormGroup className="form-group has-float-label">
                      <Label>
                       หมายเลขโทรศัพท์มือถือ
                      </Label>
                      <Field
                        className="form-control"
                        id="mobileNumber"
                        name="mobileNumber"
                        type="text"
                        maxLength="10"
                        autoComplete="off" 
                        value={mobileNumber}
                        onChange={onChangeMobileNumber}
                      />
                      {errors.mobileNumber && touched.mobileNumber && (
                        <div className="invalid-feedback d-block">
                          {errors.mobileNumber}
                        </div>
                      )}
                    </FormGroup>

                    <div className="d-flex justify-content-between align-items-center">
                      <span>มีบัญชีแล้ว? <Link href="/member/login">เข้าสู่ระบบ</Link></span>
                      <Button
                        color="primary"
                        type="button"
                        onClick={onSubmitUserData}
                        className={`btn-shadow btn-multiple-state`}
                        size="lg"
                      >
                        <span className="spinner d-inline-block">
                          <span className="bounce1" />
                          <span className="bounce2" />
                          <span className="bounce3" />
                        </span>
                        <span className="label">
                        ลงทะเบียน
                        </span>
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </Card>
        </Colxx>
      </Row>
    </div>
  )
}else if(registerState===1){
  return (
    <div className='container'>
      <Row className="h-100">
        <Colxx xxs="12" md="10" className="mx-auto my-auto">
          <Card className="auth-card">
            <div className="position-relative image-side ">
            </div>
            <div className="form-side">
              <span className="logo-single" />
              <CardTitle className="mb-4 card-title-sm">
              คำเตือน: หมาย {mobileNumber} ได้ทำการลงทะเบียนแล้ว <br/>
              คุณสามารถ เข้าระบบ ได้ด้วยหมายเลขโทรศัพท์นี้
              </CardTitle>
              <Formik>
                {({ errors, touched }) => (
                  <Form className="av-tooltip tooltip-label-bottom">
                    <div className="d-flex justify-content-between align-items-center">
                      &nbsp;
                      <Button
                        color="primary"
                        type="button"
                        onClick={()=>{
                          router.push('/member/login')
                        }}
                        className={`btn-shadow btn-multiple-state`}
                        size="lg"
                      >
                        <span className="spinner d-inline-block">
                          <span className="bounce1" />
                          <span className="bounce2" />
                          <span className="bounce3" />
                        </span>
                        <span className="label">
                        เข้าระบบ
                        </span>
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </Card>
        </Colxx>
      </Row>
    </div>
  )
}
  
}

export async function getServerSideProps(context) {
  const enhancedFetchData = await withDynamicRendering()(fetchData)
  return enhancedFetchData(context)
}

async function fetchData(context) {
  const seoData = {
    dynamicMeta: {
      ...metaRegister,
    },
  }

  return {
    props: {
      seoData,
    },
  }
}
