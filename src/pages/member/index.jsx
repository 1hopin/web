// import Icon from '@components/common/icon'
import withDynamicRendering from '@components/hoc/withDynamicRendering'
import metaLogin from '@configs/meta/login'
import { useRouter } from 'next/router'
import { getCookie } from '@utils/helper/common/cookie'
import { useEffect,useState } from 'react'
import styles from '../../components/member/member.module.scss'

export default function MemberPage() {
  const router = useRouter()
  const [authToken, setAuthToken] = useState(false)
  const [userData, setUserData] = useState(false)
  useEffect(() => {
    const authToken = getCookie('authToken')
    const cUserData = getCookie('userData')
    if(cUserData){
      const userData = JSON.parse(cUserData)
      setAuthToken(authToken);
      setUserData(userData);
    }
    
  },[])
  console.log("userData",userData)
  return (
    <div className={styles.member}>
      <img className={styles.logoimg} src='/images/logo.png' width="130" />
      <div>
        {authToken?`Authorized!`:'Unauthorized!'}<br/>
        {userData?`${userData.firstname} ${userData.lastname}`:''} 
      </div>
    </div>
  )
}

export async function getServerSideProps(context) {
  const enhancedFetchData = await withDynamicRendering()(fetchData)
  return enhancedFetchData(context)
}

async function fetchData(context) {
  const seoData = {
    dynamicMeta: {
      ...metaLogin,
    },
  }

  return {
    props: {
      seoData,
    },
  }
}
