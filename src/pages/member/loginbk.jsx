// import Icon from '@components/common/icon'
import withDynamicRendering from '@components/hoc/withDynamicRendering'
import metaLogin from '@configs/meta/login'
import { setCookie } from '@utils/helper/common/cookie'
import { useDevice } from '@utils/userAgent'
import clsx from 'clsx'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import OtpInput from 'react-otp-input'
import Image from 'next/image'

import styles from '../../components/member/login/login.module.scss'

import { requestOTP,verifyOTP } from '@features/api/otp'
import { login, register, profile } from '@features/api/member'

// LoginState Declarations
// [0] Common Case -> Request MobileNumber Input
// [1] Common Case -> Request OTP Input
// [2] New Member Case -> Request Fristname Lastname Email
// [3] Old Member Case -> Redirect to MemberPage

const otpLength = 6

export default function LoginPage() {
  const { isMobile } = useDevice()
  const { t } = useTranslation('common')
  const router = useRouter()
  const [loginState, setLoginState] = useState(0)
  const [values, setValues] = useState({
    otpToken: '',
    refNo: '',
    type: '',
    mobileNumber: '',
    otpNumber: '',
    firstname: '',
    lastname: '',
    email: '',
    errors: {
      firstname: false,
      lastname: false,
      // email: false,
    },
  })

  const handleChangeMobileNumber = (e) => {
    if (e.target.value.length <= 10 && !isNaN(e.target.value)) {
      setValues({
        ...values,
        mobileNumber: e.target.value,
        verify: { mobileNumber: e.target.value.length == 10 },
        error: false,
      })
    }
  }
  const onSubmitMobileNumber = async () => {
    if (values.mobileNumber.length == 10) {
      const result = await requestOTP(values.mobileNumber)
      console.log('onSubmitMobileNumber result',result)
      if(result?.code === 'success'){
        const otpToken = result.data?.otp.token
        const refNo = result.data?.otp.refno
        const type = result.data?.type.is_type
        setValues({
          ...values,
          otpToken:otpToken,
          refNo:refNo,
          type:type
        })
        // setCookie('otpToken', otpToken, 1000)
        setLoginState(1)

      }
      
    } else {
      setValues({
        ...values,
        error: true,
      })
    }
  }
  const handleChangeOTP = (code) => {
    if (code.length <= otpLength) {
      setValues({
        ...values,
        otpNumber: code,
        verify: { otp: code.length == otpLength },
        error: false,
      })
    }
  }
  const onSubmitOTP = async () => {
    if (values.otpNumber.length == otpLength) {
      const result = await verifyOTP(values.otpToken,values.otpNumber)
      console.log('onSubmitOTP result',result)
      if(result?.code === 'success' && values.otpNumber !== '888888'){

        if(values.type==="new"){
          setLoginState(2)
        }else{
          const loginResult = await login({
            token:values.otpToken,
            pin:values.otpNumber,
            phone_no:values.mobileNumber,
          })
          console.log('onSubmitOTP loginResult',loginResult)
          if(loginResult?.code === 'success'){
            const authToken = loginResult?.data.token
            setCookie('authToken', authToken, 1000)
            const userDataresult = await profile({
              token:authToken
            })
            if(userDataresult?.code === 'success'){
              console.log("get userData",userDataresult.data)
              setCookie('userData', JSON.stringify(userDataresult.data), 1000)
            }
            
            router.push('/member')
          }else{
            setValues({
              ...values,
              otpNumber:'',
              error: true,
            })
          }
          // setLoginState(3)
        }
        
        
      } else {
        setValues({
          ...values,
          otpNumber:'',
          error: true,
        })
      }
    } else {
      setValues({
        ...values,
        error: true,
      })
    }
  }

  const handleChangeUserData = (field) => (e) => {
    const input = e.target.value
    if (field === 'firstname') {
      setValues({
        ...values,
        firstname: input,
        verify: {
          ...values.verify,
          firstname: input.length > 0,
        },
        error: false,
      })
    } else if (field === 'lastname') {
      setValues({
        ...values,
        lastname: input,
        verify: { ...values.verify, lastname: input.length > 0 },
        error: false,
      })
    }
  }
  const onSubmitUserData = async () => {
    if (values.verify?.firstname && values.verify?.lastname) {
      const result = await register({
        firstname:values.firstname,
        lastname:values.lastname,
        phone_no:values.mobileNumber,
      })
      console.log('onSubmitUserData result',result)
      if(result?.code === 'success'){
        const authToken = result?.data.token
        setCookie('authToken', JSON.stringify(authToken), 1000)
        const userDataresult = await profile({
          token:authToken
        })
        if(userDataresult?.code === 'success'){
          console.log("get userData",userDataresult.data)
          setCookie('userData', JSON.stringify(userDataresult.data), 1000)
        }
        router.push('/member')
      } else {
        setValues({
          ...values,
          error: true,
        })
      }

      
    } else {
      setValues({
        ...values,
        error: true,
        errors: {
          firstname: values.firstname.length <= 0,
          lastname: values.lastname.length <= 0,
          // email: values.email.search('@') <= 0,
        },
      })
    }
  }

  if (loginState === 0) {
    return (
      <div className={isMobile ? styles.login_mobile : styles.login}>
        <img className={styles.logoimg} src='/images/logo.png' width="130" />
        <div className={styles.logindiv}>
            <img src='/images/icon/flag-th.png' width="40" height="40" />
            <span className={styles.loginspan}>
              &nbsp; +66 &nbsp;&nbsp;
            </span>
          <span className={clsx(styles['text-field'], values.error && styles.error)}>
            <input
              type="tel"
              value={values.mobileNumber}
              onChange={handleChangeMobileNumber}
              required
              length={10}
            />
            <span className={styles['floating-label']}>
              {values.error ? t('incorrect-mobile-number') : t('enter-mobile-number')}
            </span>
          </span>
        </div>
        <button
          className={styles.next}
          disabled={!values.verify?.mobileNumber}
          onClick={onSubmitMobileNumber}
        >
          {t('login')}
        </button>
        <div className={styles.regisdiv}>
            <span>ยังไม่มีบัญชี?</span>
            &nbsp;
            <span>ลงทะเบียน</span>
        </div>
      </div>
    )
  } else if (loginState === 1) {
    return (
      <div className={isMobile ? styles.login_mobile : styles.login}>
        <img className={styles.logoimgWithtext} src='/images/logo.png' width="130"/>
        <span className={styles['']}>กรอกรหัสยืนยัน</span>
        <p className={styles['']}>
          ระบบได้ส่งรหัส OTP ไปยัง {values.mobileNumber} <br/>
          อ้างอิงหมายเลข {values.refNo}
        </p>
        <span className={styles['floating-label','red']}>{values.error ? t('incorrect-otp') : ''}</span>
        <OtpInput
          value={values.otpNumber}
          onChange={handleChangeOTP}
          numInputs={otpLength}
          separator={<span style={{ width: '8px' }}></span>}
          isInputNum={true}
          shouldAutoFocus={true}
          inputStyle={{
            border: '2px solid #c9c8c7',
            borderRadius: '8px',
            width: '54px',
            height: '54px',
            fontSize: '1rem',
            color: '#000',
            fontWeight: '400',
            caretColor: 'blue',
          }}
          focusStyle={{
            border: '2px solid #ffa30f',
            outline: 'none',
          }}
        />

        <button className={styles.next} 
        disabled={!values.verify?.otp} 
        onClick={onSubmitOTP}>
          {t('confirm')}
        </button>
      </div>
    )
  } else if (loginState === 2) {
    return (
      <div className={isMobile ? styles.login_mobile : styles.login}>
        <img className={styles.logoimgWithtext} src='/images/logo.png' width="130"/>
        <span className={styles['']}>ลงทะเบียน</span>
        <div className={clsx(styles['text-field-register'], values.error && styles.error)}>
          <div>
            <input
              type="text"
              value={values.firstname}
              onChange={handleChangeUserData('firstname')}
              required
            />
            <span className={styles['floating-labelx']}>
              {values.errors.firstname ? t('incorrect-firstname') : t('enter-firstname')}
            </span>
          </div>

          <div>
            <input
              type="text"
              value={values.lastname}
              onChange={handleChangeUserData('lastname')}
              required
            />
            <span className={styles['floating-labelx']}>
              {values.errors.lastname ? t('incorrect-lastname') : t('enter-lastname')}
            </span>
          </div>

          {/* <div>
            <input
              type="text"
              value={values.email}
              onChange={handleChangeUserData('email')}
              required
            />
            <span className={styles['floating-labelx']}>
              {values.errors.email ? t('incorrect-email') : t('enter-email')}
            </span>
          </div> */}
        </div>

        <button
          className={styles.next}
          disabled={
            !values.verify?.firstname || !values.verify?.lastname
            // || !values.verify?.email
          }
          onClick={onSubmitUserData}
        >
          {t('save-user')}
        </button>
      </div>
    )
  }
}

export async function getServerSideProps(context) {
  const enhancedFetchData = await withDynamicRendering()(fetchData)
  return enhancedFetchData(context)
}

async function fetchData(context) {
  const seoData = {
    dynamicMeta: {
      ...metaLogin,
    },
  }

  return {
    props: {
      seoData,
    },
  }
}
