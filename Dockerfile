FROM node:18-alpine

WORKDIR /app
COPY . .
COPY .env-uat .env

RUN npm i --legacy-peer-deps
RUN npm run build

EXPOSE 9000

ENTRYPOINT ["npm", "run", "start"]
